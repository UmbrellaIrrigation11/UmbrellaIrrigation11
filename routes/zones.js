const express       = require('express');
const Zone          = require('../database/models/zone');
const Station       = require('../database/models/station');
const Schedule       = require('../database/models/schedule');
const router        = express.Router();

router.post('/zone', (request, response, next) => {
    stationJsonArray = request.body.stations; // json array of all stations for zone

    let newZone = new Zone({
        name        : request.body.name,
        description : request.body.description,
        picture     : request.body.picture,
        stations    : [],
        schedules   : []
    });
/*
    Zones save the stations object ids. Stations are created by the controllers
    themselves and put into a seperate collections. This way I can filter Stations
    that are already assigned to a zone.
*/
    if(typeof(stationJsonArray) !== 'undefined' && stationJsonArray != null) {
        stationJsonArray.forEach(function(station) {
            newZone.stations.push(station._id);
        });
    }

    Zone.addZone(newZone, (error, zone) => {
        if(error) {
            response.status(500).json({success: false, msg: 'Failed to add Zone'});
        } else {
            //The zone was created and added to the database.
            //Now need to update the stations in the stations collection
            //to reflect that they have been assigned.
            if(zone.stations != null) {
                //there are stations in this zone
                for(let id of zone.stations) {
                    Station.getStationById(id, (error, station) => {
                        if(error) {
                            response.status(500).json({success: false, msg: error});
                        } else {
                            //set each station in zone to true to show its been assigned
                            //and update station in database
                            station.isAssigned = true;
                            Station.findAndUpdateStationById(id, station, {new:true}, (error, stat) => {
                            });
                        }
                    });
                }
            }
        }
        response.status(200).json({success: true, msg: 'Zone Added'});
    });
});

router.get('/zoneList', (request, response, next) => {
    Zone.getAllZones((error, zones) => {
        if(error) {
            response.status(500).json({success: false, msg: 'Failed to get Zones'});
        } else {
            let zoneArray = [];
            zones.forEach(function(zone) {
                zoneArray.push(zone);
            });
            response.status(200).send(zoneArray);
        }
    });
});

router.put('/zone/:id', (request, response, next) => {
    let stationJsonArray    = request.body.stations; // json array of all stations
    let zoneSchedules       = request.body.schedules; //json array of all schedules

    //Since this is a put, i need to update the stations in the stations collection
    //to reflect that they are being unassigned. Then when I add the updated Stations
    //I can then assign the stations again.

    //So unassign all sations in zone, update zone, assign all stations in zone

    let updatedZone = new Zone({
        _id         : request.body._id,
        name        : request.body.name,
        description : request.body.description,
        picture     : request.body.picture,
        stations    : [],
        schedules   : []
    });

    //populate schedules
    if(typeof(zoneSchedules) !== 'undefined' && zoneSchedules != null) {
        zoneSchedules.forEach(function(schedule) {
            let newSchedule = new Schedule({
                _id         : Schedule.generateObjectId(),
                name        : schedule.name,
                startTime   : schedule.startTime,
                cycleLength : schedule.cycleLength,
                cycle       : schedule.cycle,
                isActive    : schedule.isActive,
                stations    : []
            });
            if(typeof(schedule.stations) !== 'undefined' && schedule.stations != null) {
                schedule.stations.forEach(function(station) {
                    let stat = new Station({ // create the station
                        _id             : station._id,
                        name            : station.name,
                        controlBoxId    : station.controlBoxId,
                        controllerId    : station.controllerId,
                        stationId       : station.stationId,
                        isAssigned      : station.isAssigned,
                        lat             : station.lat,
                        long            : station.long,
                        overlap         : station.overlap,
                        runTime         : station.runTime
                    });
                    //poplate stations in schedule
                    newSchedule.stations.push(stat);
                });
            }
            //populate schedules in zone
            updatedZone.schedules.push(newSchedule);
        });
    }

    //populate stations in zone and update stations collection assignment
    if(typeof(stationJsonArray) !== 'undefined' && stationJsonArray != null) {
        stationJsonArray.forEach(function(station) {
            updatedZone.stations.push(station._id);
            //update that station to show its now assigned
            Station.getStationById(station._id, (error, station) => {
                if(error) {
                    response.status(500).json({success: false, msg: error});
                } else {
                    //set each station in zone to true to show its been assigned
                    //and update station in database
                    station.isAssigned = true;
                    Station.findAndUpdateStationById(station._id, station, {new:true}, (error, stat) => {
                        //response.status(200).json({success: true, msg: 'Station'});
                    });
                }
            });
        });
    }

    Zone.findAndUpdateZone(request.params.id, updatedZone, {new:true}, (error, zone) => {
        if(error) {
            console.log(error);
            response.status(500).json({success: false, msg: error});
        } else {
            response.status(200).json(zone);
        }
    });
});

router.delete('/zone/:id', (request, response, next) => {
    //delete the zone
    Zone.findAndDeleteZoneById(request.params.id, (error, zone) => {
        if(error) {
            response.status(500).json({success: false, msg: error});
        } else {
            response.status(200).json({success: true, msg: 'Deleted Zone'});
        }
    });
});

module.exports = router;
