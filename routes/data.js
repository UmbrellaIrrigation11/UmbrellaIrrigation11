const express       = require('express');
const ControlBox    = require('../database/models/controlBox');
const Controller    = require('../database/models/controller');
const Station       = require('../database/models/station');
const router        = express.Router();

router.post('/controlBox', (request, response, next) => {
    //json array of all controllers with corresponding stations found in controlBox
    let ControllersJsonArray = request.body.controllers;

    let newControlBox = new ControlBox({
        ip              : request.body.ip,
        controlBoxId    : request.body.controlBoxId,
        longitude       : request.body.longitude,
        latitude        : request.body.latitude,
        controllers     : []
    });

    if(typeof(ControllersJsonArray) !== 'undefined' && ControllersJsonArray != null) {
            //poplate controllers
            ControllersJsonArray.forEach(function(controller) {
                let newController = new Controller({
                    controllerId : controller.controllerId,
                    stations     : []
                });
                if(typeof(controller.stations) !== 'undefined' && controller.stations != null) {
                    //poplate stations for that controller
                    controller.stations.forEach(function(station) {
                        let newStation = new Station({
                            _id             : Station.generateObjectId(),
                            name            : station.name,
                            controlBoxId    : request.body.controlBoxId,
                            controllerId    : controller.controllerId,
                            stationId       : station.stationId,
                            isAssigned      : false,
                            lat             : station.lat,
                            long            : station.long,
                            overlap         : station.overlap,
                            runTime         : station.runTime
                        });
                        //Create station in Station Collections
                        Station.addStation(newStation, (error, stat) => {
                            if(error) console.log(error);
                        });
                        //save the station object id in controller
                        newController.stations.push(newStation._id);
                    });
                }
                newControlBox.controllers.push(newController);
            });
        }

    ControlBox.addControlBox(newControlBox, (error, box) => {
        if(error) {
            response.status(500).json({success: false, msg: 'Failed to add ControlBox'});
        } else {
            response.status(200).json({success: true, msg: 'ControlBox added'});
        }
    });
});

router.get('/controlBox/:id', (request, response, next) => {
    ControlBox.getControlBoxById(request.params.id, (error, controlBox) => {
        if(error) {
            response.status(500).json({success: false, msg: 'Could not find Control Box'});
        } else {
            response.status(200).json(controlBox);
        }
    });
});

router.get('/stations', (request, response, next) => {
    Station.getAllStations((error, stations) => {
        if(error) {
            response.status(500).json({success: false, msg: 'Failed to get Stations'});
        } else {
            let stationArray = [];
            stations.forEach(function(station) {
                stationArray.push(station);
            });
            response.status(200).send(stationArray);
        }
    });
});

router.get('/controlBoxes', (request, response, next) => {
    ControlBox.getAllControlBoxes((error, controlBoxes) => {
        if(error) {
            response.status(500).json({success: false, msg: 'Failed to find Control Boxes'});
        } else {
            let controlBoxArray = [];
            controlBoxes.forEach(function(controlBox) {
                controlBoxArray.push(controlBox);
            });
            response.status(200).send(controlBoxArray);
        }
    });
});

router.put('/controlBox/:id', (request, response, next) => {
    //json array of all controllers with corresponding stations found in controlBox
    let ControllersJsonArray = request.body.controllers;

    let updatedControlBox = new ControlBox({
        _id             : request.params.id,
        ip              : request.body.ip,
        controlBoxId    : request.body.controlBoxId,
        longitude       : request.body.longitude,
        latitude        : request.body.latitude,
        controllers     : []
    });

    if(typeof(ControllersJsonArray) !== 'undefined' && ControllersJsonArray != null) {
        //poplate controllers
        ControllersJsonArray.forEach(function(controller) {
            let updatedController = new Controller({
                controllerId : controller.controllerId,
                stations     : []
            });
            if(typeof(controller.stations) !== 'undefined' && controller.stations != null) {
                //poplate stations for that controller
                controller.stations.forEach(function(station) {
                    let updatedStation = new Station({
                        _id             : station._id,
                        name            : station.name,
                        controlBoxId    : request.body.controlBoxId,
                        controllerId    : controller.controllerId,
                        stationId       : station.stationId,
                        isAssigned      : station.isAssigned,
                        lat             : station.lat,
                        long            : station.long,
                        overlap         : station.overlap,
                        runTime         : station.runTime
                    });
                    Station.findAndUpdateStationById(station._id, updatedStation, {new:true, upsert:true}, (error, stat) => {
                        if(error) console.log(error);
                    });

                    updatedController.stations.push(updatedStation._id);
                });
            }
            updatedControlBox.controllers.push(updatedController);
        });
    }
    ControlBox.findByIdAndUpdate(request.params.id, updatedControlBox, {new:true, upsert:true}, (error, controlBox) => {
        if(error) {
            response.status(500).json({success: false, msg: error});
        } else {
            response.status(200).json(controlBox);
        }
    });
});

router.delete('/controlBox/:id', (request, response, next) => {
    ControlBox.findAndDeleteControlBoxById(request.params.id, (error, box) => {
        if(error) {
            response.status(500).json({success: false, msg: 'Failed to Delete ControlBox'});
        } else {
            //delete corresponding stations
            for(let controller of box.controllers) {
                for(let id of controller.stations) {
                    Station.findAndDeleteStationById(id, (error, station) => {
                        if(error) response.status(500).json({success: false, msg: 'Failed To delete controlBox Stations'});
                    });
                }
            }
            response.status(200).json({success: true, msg: 'Control box deleted'});
        }
    });
});
module.exports = router;
