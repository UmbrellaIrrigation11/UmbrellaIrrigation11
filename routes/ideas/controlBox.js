const http = require('http');
const express = require('express');
const app = express();
  app.use(express['static'](__dirname ));


app.post('/sensors/:ID', function(request, response, next) {
    console.log(request.params.ID);
  response.status(200).json(request.params.ID + "from the pi");
});

app.post('/sensors', function(request, response, next) {
    //get all sensor data
    response.status(200).json("A bunch of sensor data");
});

app.get('*', function(req, res, next) {
  res.status(404).send('Unrecognised API call');
});


app.use(function(err, req, res, next) {
  if (req.xhr) {
    res.status(500).send('Oops, Something went wrong!');
  } else {
    next(err);
  }
});


app.listen(3001);

