const express   = require('express');
const IssueLog  = require('../database/models/issueLog');
const router    = express.Router();

router.post('/issue', (request, response, next) => {
    let newIssueLog = new IssueLog({
        name            : request.body.name,
        severity        : request.body.severity,
        dateCreated     : request.body.dateCreated,
        dateCompleted   : request.body.dateCompleted,
        controlBox      : request.body.controlBox,
        controller      : request.body.controller,
        zone            : request.body.zone,
        station         : request.body.station,
        description     : request.body.description,
        assignedUser    : request.body.assignedUser
    });

    IssueLog.addIssueLog(newIssueLog, (error, issueLog) => {
        if(error) {
            response.status(500).json({success: false, msg: 'Failed to add Issue'});
        } else {
            response.status(200).json({success: true, msg: 'Issue Added'});
        }
    });
});

router.get('/issuelist', (request, response, next) => {
    IssueLog.getAllIssueLogs((error, issues) => {
        if(error) {
            response.status(500).josn({success: false, msg: 'Failed to get Issue list'});
        } else {
            var issueLogArray = [];

            issues.forEach(function(issue) {
                issueLogArray.push(issue);
            });
            response.status(200).send(issueLogArray);
        }
    });
});

router.put('/issue/:id', (request, response, next) => {
    IssueLog.findAndUpdateIssue(request.params.id, request.body, {new:true}, (error, issue) => {
        if(error) {
            response.status(500).json({success: false, msg: error});
        } else {
            response.status(200).json(issue); //the new updated issue
        }
    });
});


router.delete('/issue/:id', (request, response, next) => {
    IssueLog.findAndDeleteIssueById(request.params.id, (error, issue) => {
        if(error) {
            response.status(500).json({success: false, msg: error});
        } else {
            response.status(200).json({success: true, msg: 'Deleted Issue'});
        }
    });
});

module.exports = router;
