const mongoose          = require('mongoose');
const Station           = require('./station');
const Schedule          = require('./schedule');

const ZoneSchema = mongoose.Schema({
    name        : { type: String, required: true  },
    description : { type: String                  },
    picture     : { type: String                  },
    stations    : [{type: mongoose.Schema.Types.ObjectId, ref: 'Station'}],
    schedules   : [Schedule.schema]
});
const Zone = module.exports = mongoose.model('Zone', ZoneSchema);

module.exports.getZoneById = function(id, callback) {
    Zone.findById(id, callback).populate('stations');
}

module.exports.addZone = function(newZone, callback) {
    newZone.save(callback);
}

module.exports.getAllZones = function(callback) {
    Zone.find({}, callback).populate('stations');
}

module.exports.findAndUpdateZone = function(id, data, options, callback) {
    Zone.findByIdAndUpdate(id, data, options, callback);
}

module.exports.findAndDeleteZoneById = function(id, callback) {
    Zone.findByIdAndRemove(id, callback);
}
