const mongoose   = require('mongoose');
const Station    = require('./station');
const Schema     = mongoose.Schema;

const ControllerSchema = new Schema({
    controllerId    : { type: String, required: true },
    stations        : [{type: mongoose.Schema.Types.ObjectId, ref: 'Station'}]
});

const Schedule = module.exports = mongoose.model('Controller', ControllerSchema);
module.exports.schema = ControllerSchema;
