const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StationSchema = new Schema({
    name            : { type: String, required: true },
    controlBoxId    : { type: String, required: true },
    controllerId    : { type: String, required: true },
    stationId       : { type: String, required: true },
    isAssigned      : { type: Boolean, required: true},
    lat             : { type: String },
    long            : { type: String },
    overlap         : { type: String },
    runTime         : { type: String }
});

const Station = module.exports = mongoose.model('Station', StationSchema);
module.exports.schema = StationSchema;

module.exports.getStationById = function(id, callback) {
    Station.findById(id, callback);
}

module.exports.addStation = function(newStation, callback) {
    newStation.save(callback);
}

module.exports.getAllStations = function(callback) {
    Station.find({}, callback);
}

module.exports.findAndUpdateStationById = function(id, data, options, callback) {
    Station.findByIdAndUpdate(id, data, options, callback);
}

module.exports.findAndDeleteStationById = function(id, callback) {
    Station.findByIdAndRemove(id, callback);
}

module.exports.generateObjectId = function() {
    return new mongoose.mongo.ObjectId();
}
