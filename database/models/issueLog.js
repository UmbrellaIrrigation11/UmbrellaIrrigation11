const mongoose = require('mongoose');
const IssueLogSchema = mongoose.Schema({
    name            : { type: String, required: true  },
    severity        : { type: String, required: true  }, //high, mediumm, low
    dateCreated     : { type: String, required: true  },
    dateCompleted   : { type: String, required: false },
    controlBox      : { type: String, required: false },
    controller      : { type: String, required: false },
    zone            : { type: String, required: false },
    station         : { type: String, required: false },
    description     : { type: String, required: false },
    assignedUser    : { type: String, required: false },
});

const IssueLog = module.exports = mongoose.model('IssueLog', IssueLogSchema);

module.exports.getIssueLogById = function(id, callback) {
    IssueLog.findById(id, callback);
}

module.exports.addIssueLog = function(newIssueLog, callback) {
    newIssueLog.save(callback);
}

module.exports.getAllIssueLogs = function(callback) {
    IssueLog.find({}, callback);
}

module.exports.findAndUpdateIssue = function(id, data, options, callback) {
    IssueLog.findByIdAndUpdate(id, data, options, callback);
}

module.exports.findAndDeleteIssueById = function(id, callback) {
    IssueLog.findByIdAndRemove(id, callback);
}


