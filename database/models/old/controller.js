const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ControllerSchema = new Schema({
   name: {
      type: String,
      // add validation
      required: [true, 'Name is required.']
   },
   stations:[{
      type: Schema.Types.ObjectId,
      ref: 'Station'
   }],
   location: {
      type: LocationSchema,
      // add validation
      required: [true, 'Locatin is required.']
   },
   // hardware info???
   uniqueId: {
      type: String,     // NOT SURE ABOUT THE TYPE
      // validation
      required: [true, 'UniqueID is required.']
   }
});

module.exports.getZoneById = function(id, callback) {
  Controller.findById(id, callback);
}

module.exports.addZone = function(newController, callback) {
  newController.save(callback);
}

module.exports.getAllZones = function(callback) {
  Controller.find({}, callback);
}

module.exports.findAndUpdateZone = function(id, data, options, callback) {
  Controller.findByIdAndUpdate(id, data, options, callback);
}

module.exports.findAndDeleteZoneById = function(id, callback) {
  Controller.findByIdAndRemove(id, callback);
}

module.exports = mongoose.model('Controller', ControllerSchema);
