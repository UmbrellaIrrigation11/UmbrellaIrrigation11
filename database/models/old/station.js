const mongoose = require('mongoose');
const LocationSchema = require('./location');
const Schema = mongoose.Schema;

const StationSchema = new Schema({
   name: {
      type: String,
      // add validation
      required: [true, 'Name is required.']
   },
   controllerIp: {
      type: String,
      // add validation
      required: [true, 'Controller is required.']
   },
   location: {
      type: LocationSchema,
      // add validation
      required: [true, 'Locatin is requiredl']
   },
});

module.exports = mongoose.model('Station', StationSchema);
