const mongoose        = require('mongoose');
const ScheduleSchema  = require('./schedule');
const Schema          = mongoose.Schema;

const ZoneSchema = new Schema({
   name: {
      type: String,
      // add validation
      required: [true, 'Name is required.']
   },
   description: String,
   picture: String,
   controllers: [{
      type: Schema.Types.ObjectId,
      ref: 'Controller'
   }],
   // Coordinates
   // Water Stats
   schedules: [ScheduleSchema]
});

const Zone = module.exports = mongoose.model('Zone', ZoneSchema);

module.exports.getZoneById = function(id, callback) {
  Zone.findById(id, callback);
}

module.exports.addZone = function(newZone, callback) {
  newZone.save(callback);
}

module.exports.getAllZones = function(callback) {
  Zone.find({}, callback);
}

module.exports.findAndUpdateZone = function(id, data, options, callback) {
  Zone.findByIdAndUpdate(id, data, options, callback);
}

module.exports.findAndDeleteZoneById = function(id, callback) {
  Zone.findByIdAndRemove(id, callback);
}
