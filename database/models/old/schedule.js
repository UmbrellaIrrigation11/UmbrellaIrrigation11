// Sub-document
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ScheduleSchema = new Schema({
   name: {
      type: String,
      // add validation
      required: [true, 'Name is required.']
   },
   startTime: Number,      // HAVE TO VERIFY IF THIS IS THE BEST WAY
   endTime: Number
   // Station Times
   // Number of Cycle Days
   // Selected Days of the Week
});

module.exports = ScheduleSchema;
