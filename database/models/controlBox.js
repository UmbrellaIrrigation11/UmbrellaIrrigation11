const mongoose   = require('mongoose');
const Controller = require('./controller');

const ControlBoxSchema = mongoose.Schema({
    ip              : { type: String, required: true },
    controlBoxId    : { type: String, required: true },
    longitude       : { type: String, required: true },
    latitude        : { type: String, required: true },
    controllers     : [Controller.schema]
});

const ControlBox = module.exports = mongoose.model('ControlBox', ControlBoxSchema);

module.exports.getControlBoxById = function(id, callback) {
    ControlBox.findById(id, callback);
}

module.exports.addControlBox = function(newControlBox, callback) {
    newControlBox.save(callback);
}

module.exports.getAllControlBoxes = function(callback) {
    ControlBox.find({}, callback);
}

module.exports.findAndUpdateControlBox = function(id, data, options, callback) {
    ControlBox.findByIdAndUpdate(id, data, options, callback);
}

module.exports.findAndDeleteControlBoxById = function(id, callback) {
    ControlBox.findByIdAndRemove(id, callback);
}
