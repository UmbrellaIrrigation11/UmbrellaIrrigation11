const mongoose = require('mongoose');
const Station  = require('./station');
const Schema = mongoose.Schema;


const ScheduleSchema = new Schema({
    name            : { type: String, required: true },
    startTime       : { type: Object, required: true },
    cycle           : { type: Object, required: true },
    cycleLength     : { type: Number, required: true },
    isActive        : { type: Boolean, required: true},
    stations        : [Station.schema]
});

const Schedule = module.exports = mongoose.model('Schedule', ScheduleSchema);
module.exports.schema = ScheduleSchema;

module.exports.getStationById = function(id, callback) {
    Schedule.findById(id, callback);
}

module.exports.addStation = function(newSchedule, callback) {
    newSchedule.save(callback);
}

module.exports.getAllStations = function(callback) {
    Schedule.find({}, callback);
}

module.exports.findAndUpdateStation = function(id, data, options, callback) {
    Schedule.findByIdAndUpdate(id, data, options, callback);
}

module.exports.findAndDeleteStationById = function(id, callback) {
    Schedule.findByIdAndRemove(id, callback);
}

module.exports.generateObjectId = function() {
    return new mongoose.mongo.ObjectId();
}
