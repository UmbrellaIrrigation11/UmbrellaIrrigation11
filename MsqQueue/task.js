#!/usr/bin/env node

var amqp = require('amqplib/callback_api');
amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        var queue = 'tasks';
        var msg = process.argv.slice(2).join(' ') || "Hello Worldd";

        //ensures rabbitMQ never loses our queue in case of server crash or
        //other problems. This must be done on both consumer and producer
        ch.assertQueue(queue, {durable: true});

        //persistent will tell rabbitmq to write msg to disk once recieved
        //in case of problems like thread crash or server crash
        ch.sendToQueue(queue, new Buffer(msg), {persistent: true});
        console.log(" [x] Sent %s", msg);
    });
    setTimeout(function() { conn.close(); process.exit(0) }, 500);
});
