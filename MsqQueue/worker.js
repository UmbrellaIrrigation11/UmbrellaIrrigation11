#!/usr/bin/env node
//Distributed Work Queues
//Uses Round-robin dispatching

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        var queue = 'tasks';

        //ensures rabbitMQ never loses out queue in case of server crash or
        //other problems. This must be done on both the consumer and producer
        ch.assertQueue(queue, {durable: true});

        //Prefetch is an attempt at fair dispatching. It looks at how many
        //unaknowledged msgs that a queue has. If there are a lot, then the
        //computation could be taking longer than others. So just give the task
        //to a different worker
        ch.prefetch(100);
        console.log(" [*] Waiting for messages in %s", queue);
        ch.consume(queue, function(msg) {
            //do work here
            var secs = msg.content.toString().split('.').length - 1;
            console.log(" [x] Received %s", msg.content.toString());
            
            setTimeout(function() {
                console.log(" [x] Done");
                //ACK the msg or else rabbitmq wont delete them and we will run
                //out of memory and die.
                ch.ack(msg);
            }, secs * 1000); //just for demonstration of a long task...
            //turn on msg acknowledgements
        }, {noAck: false});
    });
});
