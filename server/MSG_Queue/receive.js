#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

//Setting up is the same as the publisher; we open a connection and a channel,
//and declare the queue from which we're going to consume.
amqp.connect('amqp://localhost', function(err, conn) {
      conn.createChannel(function(err, ch) {
            var q = 'hello';
            ch.assertQueue(q, {durable: false});

            //Note that we declare the queue here, as well. Because we might
          //start the consumer before the publisher, we want to make sure the
          //queue exists before we try to consume messages from it.
            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
            ch.consume(q, function(msg) {
                    console.log(" [x] Received %s", msg.content.toString());
                  }, {noAck: true});
        });
});
