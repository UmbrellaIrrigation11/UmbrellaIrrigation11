#!/usr/bin/env node
var amqp = require('amqplib/callback_api');

//connect to rabbitMQ
amqp.connect('amqp://localhost', function(err, conn) {}); 

// create a channel, which is where most of the API for getting things done
// resides
amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {});
});


//declare a queue for us to send to; then we can publish a message to the
//queue Declaring a queue is idempotent - it will only be created if it doesn't
//exist already. The message content is a byte array

amqp.connect('amqp://localhost', function(err, conn) {
      conn.createChannel(function(err, ch) {
          var q = 'hello';
          ch.assertQueue(q, {durable: false});
          ch.sendToQueue(q, new Buffer('Hello World!'));
          console.log(" [x] Sent 'Hello World!'");
      });
    //close connection
    setTimeout(function() { conn.close(); process.exit(0) }, 500);
});
