webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\">\r\n    <app-splashPage *ngIf=\"!authService.loggedIn()\">\r\n        <!--(login)=\"onLogin($event)\" *ngIf=\"!authService.loggedIn()\"-->\r\n    </app-splashPage>\r\n    <div class=\"sidebar\" data-color='blue' data-image=\"\">\r\n        <app-sidebar *ngIf=\"authService.loggedIn()\"></app-sidebar> <!-- doesn't need ! -->\r\n        <div class=\"sidebar-background\" style=\"background-image: url(../assets/img/sidebar-4.jpg)\"></div>\r\n    </div>\r\n    <div *ngIf=\"authService.loggedIn()\" class=\"main-panel\">    <!-- doesn't need ! -->\r\n        <app-navbar></app-navbar>\r\n        <flash-messages></flash-messages>\r\n        \r\n        <router-outlet></router-outlet>\r\n        <div *ngIf=\"isMaps('maps')\">\r\n            <app-footer></app-footer>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_message_service__ = __webpack_require__("./src/app/services/message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = /** @class */ (function () {
    function AppComponent(location, messageService, flashMessageService, authService) {
        var _this = this;
        this.location = location;
        this.messageService = messageService;
        this.flashMessageService = flashMessageService;
        this.authService = authService;
        this.subscription = this.messageService.getMessage().subscribe(function (message) {
            _this.message = message;
            _this.flashMessageService.show(message.text, { cssClass: 'alert-success', timeout: 5000 });
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        $.material.options.autofill = true;
        $.material.init();
    };
    AppComponent.prototype.isMaps = function (path) {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        titlee = titlee.slice(1);
        if (path == titlee) {
            return false;
        }
        else {
            return true;
        }
    };
    AppComponent.prototype.ngOnDestroy = function () {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_3__services_message_service__["a" /* MessageService */],
            __WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages__["FlashMessagesService"],
            __WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_routing__ = __webpack_require__("./src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_components_module__ = __webpack_require__("./src/app/components/components.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_validate_service__ = __webpack_require__("./src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_logs_service__ = __webpack_require__("./src/app/services/logs.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__guards_auth_guard__ = __webpack_require__("./src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_message_service__ = __webpack_require__("./src/app/services/message.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_users_service__ = __webpack_require__("./src/app/services/users.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_controlBoxes_service__ = __webpack_require__("./src/app/services/controlBoxes.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_irrigationZones_service__ = __webpack_require__("./src/app/services/irrigationZones.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_maps_maps_component__ = __webpack_require__("./src/app/components/maps/maps.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_notifications_notifications_component__ = __webpack_require__("./src/app/components/notifications/notifications.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_events_events_component__ = __webpack_require__("./src/app/components/events/events.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_schedule_schedule_component__ = __webpack_require__("./src/app/components/schedule/schedule.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_weather_weather_component__ = __webpack_require__("./src/app/components/weather/weather.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_roles_roles_component__ = __webpack_require__("./src/app/components/roles/roles.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_man_operations_man_operations_component__ = __webpack_require__("./src/app/components/man-operations/man-operations.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_reports_reports_component__ = __webpack_require__("./src/app/components/reports/reports.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__components_register_register_component__ = __webpack_require__("./src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__components_splashPage_splashPage_component__ = __webpack_require__("./src/app/components/splashPage/splashPage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__components_irrigation_zones_irrigation_zones_component__ = __webpack_require__("./src/app/components/irrigation-zones/irrigation-zones.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__angular_material_select__ = __webpack_require__("./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__angular_material_card__ = __webpack_require__("./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__angular_material_expansion__ = __webpack_require__("./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__angular_material_chips__ = __webpack_require__("./node_modules/@angular/material/esm5/chips.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__angular_material_list__ = __webpack_require__("./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__angular_material_dialog__ = __webpack_require__("./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__angular_material_button__ = __webpack_require__("./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__angular_material_form_field__ = __webpack_require__("./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__angular_material_stepper__ = __webpack_require__("./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__angular_material_grid_list__ = __webpack_require__("./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__angular_material_divider__ = __webpack_require__("./node_modules/@angular/material/esm5/divider.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42_ng2_dragula__ = __webpack_require__("./node_modules/ng2-dragula/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42_ng2_dragula___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_42_ng2_dragula__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43_ngx_popover__ = __webpack_require__("./node_modules/ngx-popover/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43_ngx_popover___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_43_ngx_popover__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









//services








//tabs are below



























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_17__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_20__components_events_events_component__["a" /* EventsComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_schedule_schedule_component__["a" /* ScheduleComponent */],
                __WEBPACK_IMPORTED_MODULE_22__components_weather_weather_component__["a" /* WeatherComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_roles_roles_component__["a" /* RolesComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_man_operations_man_operations_component__["a" /* ManOperationsComponent */],
                __WEBPACK_IMPORTED_MODULE_18__components_maps_maps_component__["a" /* MapsComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_notifications_notifications_component__["b" /* NotificationsComponent */],
                __WEBPACK_IMPORTED_MODULE_25__components_reports_reports_component__["a" /* ReportsComponent */],
                __WEBPACK_IMPORTED_MODULE_26__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_notifications_notifications_component__["a" /* CustomLogDialogOverview */],
                __WEBPACK_IMPORTED_MODULE_27__components_splashPage_splashPage_component__["a" /* SplashPageComponent */],
                __WEBPACK_IMPORTED_MODULE_28__components_irrigation_zones_irrigation_zones_component__["b" /* IrrigationZonesComponent */],
                __WEBPACK_IMPORTED_MODULE_28__components_irrigation_zones_irrigation_zones_component__["a" /* DialogOverviewZoneInfoDialog */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_30__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["j" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_6__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_5__app_routing__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_29__angular_material__["b" /* MatAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_29__angular_material__["e" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_31__angular_material_select__["a" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_33__angular_material_expansion__["a" /* MatExpansionModule */],
                __WEBPACK_IMPORTED_MODULE_32__angular_material_card__["a" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_34__angular_material_chips__["a" /* MatChipsModule */],
                __WEBPACK_IMPORTED_MODULE_35__angular_material_list__["a" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_36__angular_material_dialog__["c" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_37__angular_material_button__["a" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_38__angular_material_form_field__["c" /* MatFormFieldModule */],
                __WEBPACK_IMPORTED_MODULE_39__angular_material_stepper__["a" /* MatStepperModule */],
                __WEBPACK_IMPORTED_MODULE_40__angular_material_grid_list__["a" /* MatGridListModule */],
                __WEBPACK_IMPORTED_MODULE_41__angular_material_divider__["a" /* MatDividerModule */],
                __WEBPACK_IMPORTED_MODULE_42_ng2_dragula__["DragulaModule"],
                __WEBPACK_IMPORTED_MODULE_43_ngx_popover__["PopoverModule"],
                __WEBPACK_IMPORTED_MODULE_7_angular2_flash_messages__["FlashMessagesModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_routing__["b" /* routes */])
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__services_validate_service__["a" /* ValidateService */],
                __WEBPACK_IMPORTED_MODULE_10__services_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_12__guards_auth_guard__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_11__services_logs_service__["a" /* LogService */],
                __WEBPACK_IMPORTED_MODULE_13__services_message_service__["a" /* MessageService */],
                __WEBPACK_IMPORTED_MODULE_14__services_users_service__["a" /* UsersService */],
                __WEBPACK_IMPORTED_MODULE_15__services_controlBoxes_service__["a" /* ControlBoxService */],
                __WEBPACK_IMPORTED_MODULE_16__services_irrigationZones_service__["a" /* IrrigationZoneService */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_19__components_notifications_notifications_component__["a" /* CustomLogDialogOverview */],
                __WEBPACK_IMPORTED_MODULE_28__components_irrigation_zones_irrigation_zones_component__["a" /* DialogOverviewZoneInfoDialog */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_17__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_maps_maps_component__ = __webpack_require__("./src/app/components/maps/maps.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_notifications_notifications_component__ = __webpack_require__("./src/app/components/notifications/notifications.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_events_events_component__ = __webpack_require__("./src/app/components/events/events.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_schedule_schedule_component__ = __webpack_require__("./src/app/components/schedule/schedule.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_weather_weather_component__ = __webpack_require__("./src/app/components/weather/weather.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_roles_roles_component__ = __webpack_require__("./src/app/components/roles/roles.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_man_operations_man_operations_component__ = __webpack_require__("./src/app/components/man-operations/man-operations.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_reports_reports_component__ = __webpack_require__("./src/app/components/reports/reports.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_register_register_component__ = __webpack_require__("./src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_splashPage_splashPage_component__ = __webpack_require__("./src/app/components/splashPage/splashPage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_irrigation_zones_irrigation_zones_component__ = __webpack_require__("./src/app/components/irrigation-zones/irrigation-zones.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__guards_auth_guard__ = __webpack_require__("./src/app/guards/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















/*
*const appRoutes: Routes = [
{ path:'',          component: HomeComponent      },
{ path:'register',  component: RegisterComponent  },
{ path:'login',     component: LoginComponent     },
{ path:'dashboard', component: DashboardComponent , canActivate:[AuthGuard] }, //protect pages if not logged in
{ path:'profile',   component: ProfileComponent   , canActivate:[AuthGuard] },
]
* */
/*
* To hide certain buttons or something, do this in the html
* <ul class="nav navbar-nav navbar-left">
*      <li [routerLinkActive]="['active']" [routerLinkActiveOptions] = "{exact:true}"><a [routerLink]="['/']">Home</a></li>
* </ul>
*
*  <ul class="nav navbar-nav navbar-right">
*      <li *ngIf="authService.loggedIn()" [routerLinkActive]="['active']" [routerLinkActiveOptions] = "{exact:true}"><a[routerLink]="['/dashboard']">Dashboard</a></li>
*      <li *ngIf="authService.loggedIn()" [routerLinkActive]="['active']" [routerLinkActiveOptions] = "{exact:true}"><a[routerLink]="['/profile']">Profile</a></li>
*
* */
//also note that logout is in navbar html
var routes = [
    { path: 'notifications', component: __WEBPACK_IMPORTED_MODULE_5__components_notifications_notifications_component__["b" /* NotificationsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_15__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_13__components_splashPage_splashPage_component__["a" /* SplashPageComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_12__components_register_register_component__["a" /* RegisterComponent */] },
    { path: 'events', component: __WEBPACK_IMPORTED_MODULE_6__components_events_events_component__["a" /* EventsComponent */] },
    { path: 'man-operations', component: __WEBPACK_IMPORTED_MODULE_10__components_man_operations_man_operations_component__["a" /* ManOperationsComponent */] },
    { path: 'weather', component: __WEBPACK_IMPORTED_MODULE_8__components_weather_weather_component__["a" /* WeatherComponent */] },
    { path: 'schedule', component: __WEBPACK_IMPORTED_MODULE_7__components_schedule_schedule_component__["a" /* ScheduleComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_15__guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'roles', component: __WEBPACK_IMPORTED_MODULE_9__components_roles_roles_component__["a" /* RolesComponent */] },
    { path: 'maps', component: __WEBPACK_IMPORTED_MODULE_4__components_maps_maps_component__["a" /* MapsComponent */] },
    { path: 'reports', component: __WEBPACK_IMPORTED_MODULE_11__components_reports_reports_component__["a" /* ReportsComponent */] },
    { path: 'irrigation-zones', component: __WEBPACK_IMPORTED_MODULE_14__components_irrigation_zones_irrigation_zones_component__["b" /* IrrigationZonesComponent */] },
    { path: '**', redirectTo: 'notifications', pathMatch: 'full' },
    { path: '', redirectTo: 'notifications', pathMatch: 'full' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* RouterModule */].forRoot(routes)
            ],
            exports: [],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/components.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__footer_footer_component__ = __webpack_require__("./src/app/components/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__navbar_navbar_component__ = __webpack_require__("./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sidebar_sidebar_component__ = __webpack_require__("./src/app/components/sidebar/sidebar.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_4__navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_5__sidebar_sidebar_component__["b" /* SidebarComponent */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_4__navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_5__sidebar_sidebar_component__["b" /* SidebarComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "./src/app/components/events/events.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/events/events.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n    \r\n</div>"

/***/ }),

/***/ "./src/app/components/events/events.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EventsComponent = /** @class */ (function () {
    function EventsComponent() {
    }
    EventsComponent.prototype.ngOnInit = function () {
    };
    EventsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-evnts',
            template: __webpack_require__("./src/app/components/events/events.component.html"),
            styles: [__webpack_require__("./src/app/components/events/events.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], EventsComponent);
    return EventsComponent;
}());



/***/ }),

/***/ "./src/app/components/footer/footer.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer>\r\n    <div class=\"container-fluid\">\r\n        \r\n        <p class=\"copyright pull-right\">\r\n            &copy; {{test | date: 'yyyy'}} <a><strong>Umbrella Irrigation</strong></a>, We got you covered. \r\n        </p>\r\n    </div>\r\n</footer>\r\n"

/***/ }),

/***/ "./src/app/components/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
        this.test = new Date();
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("./src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__("./src/app/components/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/components/irrigation-zones/dialog-overview-zone-info-dialog.html":
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>\r\n    <mat-list>\r\n        <mat-list-item> {{data.name}}</mat-list-item>\r\n    </mat-list>\r\n</h1>\r\n\r\n<div mat-dialog-content>\r\n    <mat-list>\r\n        <mat-list-item> Description: {{data.description}}</mat-list-item>\r\n        <mat-divider></mat-divider>\r\n\r\n        <mat-list-item>\r\n            <mat-expansion-panel (opened)=\"panelOpenState = true\"\r\n            (closed)=\"panelOpenState = false\">\r\n            <mat-expansion-panel-header class=\"right-aligned-header\">\r\n                <mat-panel-title>\r\n                    <h5 mat-line>Stations</h5>\r\n                </mat-panel-title>\r\n            </mat-expansion-panel-header>\r\n            <mat-list>\r\n                <mat-list-item *ngFor=\"let station of data.stations\"> {{station.name}}</mat-list-item>\r\n            </mat-list>\r\n        </mat-expansion-panel>\r\n    </mat-list-item>\r\n\r\n    <mat-divider></mat-divider>\r\n\r\n\r\n    <mat-list-item>\r\n        <mat-expansion-panel (opened)=\"panelOpenState = true\"\r\n        (closed)=\"panelOpenState = false\">\r\n        <mat-expansion-panel-header class=\"right-aligned-header\">\r\n            <mat-panel-title>\r\n                <h5 mat-line>Schedules</h5>\r\n            </mat-panel-title>\r\n        </mat-expansion-panel-header>\r\n        <mat-list>\r\n            <mat-list-item *ngFor=\"let schedule of data.schedules\"> {{schedule.name}}</mat-list-item>\r\n        </mat-list>\r\n    </mat-expansion-panel>\r\n</mat-list-item>\r\n"

/***/ }),

/***/ "./src/app/components/irrigation-zones/irrigation-zones.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n    <button mat-raised-button color=\"warn\" (click)=\"createZone(); stepper.selectedIndex =0;\" id=\"customButton\">Create Zone</button>\r\n\r\n<mat-card *ngIf=isCreating>\r\n    <mat-horizontal-stepper [linear]=\"true\" #stepper=\"matHorizontalStepper\">\r\n        <mat-step [stepControl]=\"firstFormGroup\">\r\n            <form [formGroup]=\"firstFormGroup\">\r\n              <ng-template matStepLabel>Name the Zone</ng-template>\r\n                  <mat-form-field>\r\n                    <input matInput placeholder=\"Zone Name\" formControlName=\"firstCtrl\" required>\r\n                  </mat-form-field>\r\n              <div>\r\n                <button mat-button matStepperNext>Next</button>\r\n              </div>\r\n            </form>\r\n\r\n        </mat-step>\r\n          <mat-step [stepControl]=\"secondFormGroup\">\r\n            <form [formGroup]=\"secondFormGroup\">\r\n                <ng-template matStepLabel>Zone Description</ng-template>\r\n                  <mat-form-field>\r\n                    <input matInput placeholder=\"Description\" formControlName=\"secondCtrl\" required>\r\n                  </mat-form-field>\r\n                      <div>\r\n                        <button mat-button matStepperPrevious>Back</button>\r\n                        <button mat-button matStepperNext>Next</button>\r\n                      </div>\r\n            </form>\r\n        </mat-step>\r\n\r\n      <mat-step [stepControl]=\"thirdFormGroup\">\r\n        <form [formGroup]=\"thirdFormGroup\">\r\n            <ng-template matStepLabel>Zone Description</ng-template>\r\n\r\n            <mat-grid-list cols=\"2\" rowHeight=\"2:1\">\r\n            <mat-grid-tile class=\"bagContainer\">\r\n                <mat-list class='container' [dragula]='\"first-bag\"' [dragulaModel]='unassignedStations'>\r\n                    <mat-list-item *ngFor='let station of unassignedStations' ng-draggable=\"true\">\r\n                          {{ station.stationId }}\r\n                    </mat-list-item>\r\n                </mat-list>\r\n            </mat-grid-tile>\r\n\r\n            <mat-grid-tile class=\"bagContainer\">\r\n                <mat-list class='container' [dragula]='\"first-bag\"' [dragulaModel]='zoneStationsArray'>\r\n                    <mat-list-item  *ngFor='let station of zoneStationsArray'ng-draggable=\"true\">\r\n                        {{ station.stationId }}\r\n                    </mat-list-item>\r\n                </mat-list>\r\n            </mat-grid-tile>\r\n            </mat-grid-list>\r\n            <input matInput formControlName=\"thirdCtrl\" [disabled]> <!-- This is  a cheap hack cuz of the form im using -->\r\n                  <div>\r\n                    <button mat-button matStepperPrevious>Back</button>\r\n                    <button mat-button matStepperNext>Next</button>\r\n                  </div>\r\n        </form>\r\n    </mat-step>\r\n\r\n        <mat-step>\r\n            <ng-template matStepLabel>Confirm</ng-template>\r\n                Is Everything good?\r\n            <div>\r\n              <button mat-button matStepperPrevious>Back</button>\r\n              <button mat-button (click)=\"submitZone(); stepper.reset()\">Save</button>\r\n            </div>\r\n      </mat-step>\r\n  </mat-horizontal-stepper>\r\n</mat-card>\r\n\r\n\r\n\r\n\r\n<mat-card>\r\n    <mat-card-header>\r\n        <mat-card-title> Zones </mat-card-title>\r\n    </mat-card-header>\r\n    <mat-card-content>\r\n        <mat-accordion>\r\n            <mat-list>\r\n            <mat-list-item *ngFor=\"let zone of irrigationZones | async\">\r\n                <button mat-raised-button color=\"primary\" (click)=\"openDialog(zone)\">{{zone.name}}</button>\r\n            </mat-list-item>\r\n        </mat-list>\r\n        </mat-accordion>\r\n    </mat-card-content>\r\n</mat-card>\r\n\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/irrigation-zones/irrigation-zones.component.scss":
/***/ (function(module, exports) {

module.exports = "/*Autocomplete */\n.example-form {\n  background-color: white;\n  min-width: 150px;\n  max-width: 390px;\n  margin-bottom: 15px;\n  width: 100%; }\n/*Autocomplete */\n.example-full-width {\n  font-size: 18px;\n  width: 100%;\n  margin-top: 2px;\n  margin-bottom: -15px; }\n.row {\n  margin-left: 10px; }\nlabel {\n  color: #3C4858;\n  font-weight: 300;\n  font-family: \"Roboto\", \"Helvetica\", \"Arial\", sans-serif; }\n#amRadio {\n  margin-left: 5px; }\n.availableButton {\n  -webkit-user-drag: element;\n  -khtml-user-drag: element;\n  width: block; }\n#availableButton:hover {\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-style: bold;\n  -webkit-text-decoration-color: none;\n          text-decoration-color: none;\n  border-color: #9dc1ff;\n  border-width: 3px; }\n#droppedButton {\n  width: 100%;\n  display: block;\n  font-size: 12pt;\n  font-style: bold; }\n#droppedButton:hover {\n  padding-top: 15px;\n  padding-bottom: 15px;\n  font-style: bold;\n  -webkit-text-decoration-color: none;\n          text-decoration-color: none;\n  border-style: solid;\n  border-width: 3px;\n  border-color: #9dc1ff; }\n#displayZoneButtons {\n  border: none; }\n#hourInput {\n  width: 50px; }\n#minuteInput {\n  width: 75px; }\n#nameLabel {\n  float: left;\n  margin-left: 15px; }\n#offSetInput {\n  width: 25%;\n  margin-left: 4em; }\n#runTime {\n  width: 25%;\n  float: right; }\n#runTimeLabel {\n  float: right;\n  margin-right: 15px; }\n#zone-summary {\n  /*background-color: rgb(196, 196, 196);*/\n  height: auto;\n  width: 390px;\n  text-align: center; }\n#stationList {\n  background-color: white;\n  width: 250px;\n  text-align: center; }\n#scheduleList {\n  background-color: white;\n  border-bottom: medium;\n  border-bottom-color: #eeeeee;\n  width: 400px;\n  text-align: center; }\n#scheduleListTitle {\n  border-style: none; }\n#stationDescription {\n  border: visible;\n  border-top-style: solid;\n  border-top-width: 5px;\n  border-top-color: #eeeeee;\n  font-size: 14pt;\n  text-align: center; }\n#saveButton {\n  text-align: center;\n  block-size: auto; }\n#saveScheduleButton {\n  -ms-flex-item-align: auto;\n      -ms-grid-row-align: auto;\n      align-self: auto; }\n#savedSchedule {\n  background-color: white;\n  height: auto;\n  width: 250px;\n  text-align: center; }\n.scheduleLabels {\n  text-indent: 1em;\n  border-top-width: 2px;\n  border-top-style: solid;\n  border-top-color: #eeeeee;\n  text-align: left;\n  display: block; }\n.scheduleListLabels {\n  text-indent: 10em; }\n:host ::ng-deep .cycleDay {\n  text-decoration: none;\n  padding: 5px;\n  color: #3C4858;\n  font-weight: 300;\n  font-family: \"Roboto\", \"Helvetica\", \"Arial\", sans-serif; }\n.cycleLabels {\n  padding-top: 5px;\n  padding-right: 1em; }\n#startTimeForm {\n  text-align: center;\n  padding-left: 25px; }\n.currentScheduleLabels {\n  padding-right: 3em;\n  text-align: left;\n  display: inline-block; }\n#currentScheduleBlock {\n  border-top-width: 2px;\n  border-top-style: solid;\n  border-top-color: #eeeeee;\n  margin-bottom: -2px; }\n#viewScheduleButtons:hover {\n  border-width: 3px;\n  border-color: #9dc1ff;\n  padding-top: 15px;\n  padding-right: 0px;\n  padding-left: 0px;\n  padding-bottom: 15px; }\n.viewButtonLabels {\n  float: left;\n  padding-right: 3em; }\n#viewButtonLabelTimer {\n  padding-left: 0px;\n  float: right;\n  padding-right: 0px; }\n#viewScheduleButtons {\n  text-decoration: none; }\n/*crd zone-summary text */\n.mdc-card__supporting-text {\n  text-align: center;\n  /*padding-bottom: 10px;*/ }\n.mdc-card {\n  text-align: center;\n  border: hidden;\n  height: auto;\n  background: white; }\n.mdc-card__primary {\n  border-bottom: solid;\n  border-bottom-color: #d4d3d3;\n  margin-top: -15px;\n  height: auto; }\n.mdc-card__actions {\n  padding-bottom: 10px; }\nimg {\n  height: 300px;\n  width: 100%;\n  padding-top: 5px;\n  padding-left: 5px;\n  padding-right: 5px; }\n.list-group {\n  max-height: 300px;\n  overflow-y: auto; }\n#customButton {\n  margin-left: 50px; }\n.mat-card {\n  margin-left: 25px; }\n.bagContainer {\n  border: solid;\n  border-color: black;\n  border-width: 1px; }\n"

/***/ }),

/***/ "./src/app/components/irrigation-zones/irrigation-zones.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return IrrigationZonesComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogOverviewZoneInfoDialog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_dragula_ng2_dragula__ = __webpack_require__("./node_modules/ng2-dragula/ng2-dragula.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_dragula_ng2_dragula___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_dragula_ng2_dragula__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_irrigationZones_service__ = __webpack_require__("./src/app/services/irrigationZones.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_controlBoxes_service__ = __webpack_require__("./src/app/services/controlBoxes.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var IrrigationZonesComponent = /** @class */ (function () {
    function IrrigationZonesComponent(irrigationZoneService, controlBoxService, formBuilder, DragulaService, dialog) {
        this.irrigationZoneService = irrigationZoneService;
        this.controlBoxService = controlBoxService;
        this.formBuilder = formBuilder;
        this.DragulaService = DragulaService;
        this.dialog = dialog;
        this.zoneStationsArray = [];
        this.unassignedStations = [];
        this.isCreating = false;
        DragulaService.setOptions('first-bag', {});
    }
    IrrigationZonesComponent.prototype.ngOnInit = function () {
        this.refreshForms();
    };
    IrrigationZonesComponent.prototype.createZone = function () {
        var _this = this;
        this.refreshForms();
        if (this.isCreating) {
            this.zoneStationsArray = [];
            this.stepper.reset();
            //this.stepper.selectedIndex = 1;
        }
        this.unassignedStations = [];
        this.isCreating = true;
        this.fieldStations.subscribe(function (stations) { return (_this.unassignedStations = stations); });
    };
    IrrigationZonesComponent.prototype.submitZone = function () {
        var zone = {
            name: this.firstFormGroup.get('firstCtrl').value,
            description: this.secondFormGroup.get('secondCtrl').value,
            stations: this.zoneStationsArray
        };
        this.irrigationZoneService.create(zone);
        this.refreshForms();
    };
    IrrigationZonesComponent.prototype.refreshForms = function () {
        this.firstFormGroup = this.formBuilder.group({
            firstCtrl: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["k" /* Validators */].required]
        });
        this.secondFormGroup = this.formBuilder.group({
            secondCtrl: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["k" /* Validators */].required]
        });
        this.thirdFormGroup = this.formBuilder.group({
            thirdCtrl: ['   ']
        });
        this.isCreating = false;
        this.zoneStationsArray = [];
        this.irrigationZones = this.irrigationZoneService.irrigationZones;
        this.irrigationZoneService.loadAll();
        this.controlBoxService.loadAllStations();
        this.irrigationZoneService.loadAll();
        this.fieldStations = this.controlBoxService.stations
            .map(function (stations) { return stations.filter(function (station) { return (!station.isAssigned); }); }, function (error) { return console.log(error); });
    };
    IrrigationZonesComponent.prototype.openDialog = function (zone) {
        var dialogRef = this.dialog.open(DialogOverviewZoneInfoDialog, {
            width: '500px',
            data: {
                name: zone.name,
                description: zone.description,
                picture: zone.picture,
                stations: zone.stations,
                schedules: zone.schedules
            }
        });
    };
    IrrigationZonesComponent.prototype.ngOnDestroy = function () {
        this.DragulaService.destroy('first-bag');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('stepper'),
        __metadata("design:type", Object)
    ], IrrigationZonesComponent.prototype, "stepper", void 0);
    IrrigationZonesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-irrigation-zones',
            template: __webpack_require__("./src/app/components/irrigation-zones/irrigation-zones.component.html"),
            styles: [__webpack_require__("./src/app/components/irrigation-zones/irrigation-zones.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__services_irrigationZones_service__["a" /* IrrigationZoneService */],
            __WEBPACK_IMPORTED_MODULE_5__services_controlBoxes_service__["a" /* ControlBoxService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3_ng2_dragula_ng2_dragula__["DragulaService"],
            __WEBPACK_IMPORTED_MODULE_2__angular_material__["c" /* MatDialog */]])
    ], IrrigationZonesComponent);
    return IrrigationZonesComponent;
}());

var DialogOverviewZoneInfoDialog = /** @class */ (function () {
    function DialogOverviewZoneInfoDialog(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    DialogOverviewZoneInfoDialog.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    DialogOverviewZoneInfoDialog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dialog-overview-zone-info',
            template: __webpack_require__("./src/app/components/irrigation-zones/dialog-overview-zone-info-dialog.html"),
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_material__["d" /* MatDialogRef */], Object])
    ], DialogOverviewZoneInfoDialog);
    return DialogOverviewZoneInfoDialog;
}());



/***/ }),

/***/ "./src/app/components/man-operations/man-operations.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/man-operations/man-operations.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n    \r\n</div>"

/***/ }),

/***/ "./src/app/components/man-operations/man-operations.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManOperationsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ManOperationsComponent = /** @class */ (function () {
    function ManOperationsComponent() {
    }
    ManOperationsComponent.prototype.ngOnInit = function () {
    };
    ManOperationsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-man-operations',
            template: __webpack_require__("./src/app/components/man-operations/man-operations.component.html"),
            styles: [__webpack_require__("./src/app/components/man-operations/man-operations.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ManOperationsComponent);
    return ManOperationsComponent;
}());



/***/ }),

/***/ "./src/app/components/maps/maps.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/maps/maps.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"map\"></div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/components/maps/maps.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MapsComponent = /** @class */ (function () {
    function MapsComponent() {
    }
    MapsComponent.prototype.ngOnInit = function () {
        var myLatlng = new google.maps.LatLng(40.748817, -73.985428);
        var mapOptions = {
            zoom: 13,
            center: myLatlng,
            scrollwheel: false,
            styles: [
                { 'featureType': 'water', 'stylers': [{ 'saturation': 43 }, { 'lightness': -11 }, { 'hue': '#0088ff' }] },
                { 'featureType': 'road', 'elementType': 'geometry.fill', 'stylers': [{ 'hue': '#ff0000' },
                        { 'saturation': -100 }, { 'lightness': 99 }] },
                { 'featureType': 'road', 'elementType': 'geometry.stroke', 'stylers': [{ 'color': '#808080' },
                        { 'lightness': 54 }] },
                { 'featureType': 'landscape.man_made', 'elementType': 'geometry.fill', 'stylers': [{ 'color': '#ece2d9' }] },
                { 'featureType': 'poi.park', 'elementType': 'geometry.fill', 'stylers': [{ 'color': '#ccdca1' }] },
                { 'featureType': 'road', 'elementType': 'labels.text.fill', 'stylers': [{ 'color': '#767676' }] },
                { 'featureType': 'road', 'elementType': 'labels.text.stroke', 'stylers': [{ 'color': '#ffffff' }] },
                { 'featureType': 'poi', 'stylers': [{ 'visibility': 'off' }] },
                { 'featureType': 'landscape.natural', 'elementType': 'geometry.fill', 'stylers': [{ 'visibility': 'on' },
                        { 'color': '#b8cb93' }] },
                { 'featureType': 'poi.park', 'stylers': [{ 'visibility': 'on' }] },
                { 'featureType': 'poi.sports_complex', 'stylers': [{ 'visibility': 'on' }] },
                { 'featureType': 'poi.medical', 'stylers': [{ 'visibility': 'on' }] },
                { 'featureType': 'poi.business', 'stylers': [{ 'visibility': 'simplified' }] }
            ]
        };
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var Marker = new google.maps.Marker({
            position: myLatlng,
            title: 'Hello World!'
        });
        // To add the marker to the map, call setMap();
        Marker.setMap(map);
    };
    MapsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-maps',
            template: __webpack_require__("./src/app/components/maps/maps.component.html"),
            styles: [__webpack_require__("./src/app/components/maps/maps.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MapsComponent);
    return MapsComponent;
}());



/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports) {

module.exports = "#navbar{\r\n    padding-right: 15px;\r\n}\r\n\r\n#logout{\r\n    font-size: 48px;\r\n    display: inline-block;\r\n    margin-right: 15px;\r\n    word-wrap: inherit;\r\n}\r\n\r\n#logoutWord{\r\n    word-wrap: normal;\r\n}"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-transparent navbar-absolute\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"navbar-header\">\r\n            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" (click)=\"sidebarToggle()\">\r\n                <span class=\"sr-only\">Toggle navigation</span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <a class=\"navbar-brand\" href=\"#\">{{getTitle()}}</a>\r\n        </div>\r\n        <div class=\"collapse navbar-collapse\">\r\n            <ul class=\"nav navbar-nav navbar-right\">\r\n                <li>\r\n                <li class=\"dropdown\">\r\n                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\r\n                        <i class=\"material-icons\">notifications</i>\r\n                        <span class=\"notification\">5</span>\r\n                        <p class=\"hidden-lg hidden-md\">Notifications</p>\r\n                    </a>\r\n                    <ul class=\"dropdown-menu\">\r\n                        <li><a href=\"#\">Jacaranda controller Low water pressure</a></li>\r\n                        <li><a href=\"#\">Winter schedule enabled</a></li>\r\n                        <li><a href=\"#\">Rain on Thursday</a></li>\r\n                        <li><a href=\"#\">Another Notification</a></li>\r\n                        <li><a href=\"#\">Another One</a></li>\r\n                    </ul>\r\n                </li>\r\n                <li>\r\n                    <a href=\"#pablo\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\r\n                       <i class=\"material-icons\">person</i>\r\n                       <p class=\"hidden-lg hidden-md\">Profile</p>\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n\r\n            <form class=\"navbar-form navbar-right\" role=\"search\">\r\n                <div class=\"form-group form-black is-empty\">\r\n                    <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\r\n                    <span class=\"material-input\"></span>\r\n                </div>\r\n                <button type=\"submit\" class=\"btn btn-white btn-round btn-just-icon\">\r\n                    <i class=\"material-icons\">search</i><div class=\"ripple-container\"></div>\r\n                </button>\r\n            </form>\r\n\r\n            <div id=\"navbar\"><!-- class=\"collapse navbar-collapse\"-->     \r\n\r\n                <ul class=\"nav navbar-nav navbar-right\">\r\n      \r\n                  <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\"><a [routerLink]=\"['/login']\">Login</a></li>\r\n                  <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\"><a [routerLink]=\"['/register']\">Register</a></li>\r\n                  <li *ngIf=\"authService.loggedIn()\"><a (click)=\"onLogoutClick()\" href=\"#\" id=\"logoutWord\">logout</a></li>\r\n      \r\n                </ul>\r\n              </div><!--/.nav-collapse -->\r\n        </div>\r\n    </div>\r\n    \r\n<!-- <div class=\"container\">\r\n        <div id=\"navbar\" class=\"collapse navbar-collapse\">     \r\n\r\n          <ul class=\"nav navbar-nav navbar-right\">\r\n\r\n            <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\"><a [routerLink]=\"['/login']\">Login</a></li>\r\n            <li *ngIf=\"!authService.loggedIn()\" [routerLinkActive]=\"['active']\" [routerLinkActiveOptions] = \"{exact:true}\"><a [routerLink]=\"['/register']\">Register</a></li>\r\n            <li *ngIf=\"authService.loggedIn()\"><a (click)=\"onLogoutClick()\" href=\"#\">Logout</a></li>\r\n\r\n            \r\n                              <popover-content #myPopover title=\"Controller: {{ station.controller }}, Station: {{ station.name }}\" placement=\"above\" [closeOnMouseOutside]=\"true\">\r\n                    <button type=\"button\" class=\"btn btn-danger\" (ng-click)=\"onDeleteStationClick(station.controller, station.name)\">Delete</button>\r\n                  </popover-content>\r\n\r\n\r\n\r\n          </ul>\r\n        </div><!--/.nav-collapse -->\r\n      <!--/div> -->\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sidebar_sidebar_component__ = __webpack_require__("./src/app/components/sidebar/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(location, element, authService, router, flashMessageService) {
        this.element = element;
        this.authService = authService;
        this.router = router;
        this.flashMessageService = flashMessageService;
        this.location = location;
        this.sidebarVisible = false;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        this.listTitles = __WEBPACK_IMPORTED_MODULE_1__sidebar_sidebar_component__["a" /* ROUTES */].filter(function (listTitle) { return listTitle; });
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    };
    NavbarComponent.prototype.sidebarOpen = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        this.sidebarVisible = true;
    };
    ;
    NavbarComponent.prototype.sidebarClose = function () {
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    ;
    NavbarComponent.prototype.sidebarToggle = function () {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        }
        else {
            this.sidebarClose();
        }
    };
    ;
    NavbarComponent.prototype.getTitle = function () {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(2);
        }
        titlee = titlee.split('/').pop();
        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return '';
    };
    NavbarComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        this.flashMessageService.show('Logged Out!', { cssClass: 'alert-success', timeout: 5000 });
        this.router.navigate(['/login']);
        return false;
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("./src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_3__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5_angular2_flash_messages__["FlashMessagesService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/components/notifications/custom-log-dialog-overview.html":
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title> Create Issue</h1>\r\n<div mat-dialog-content>\r\n    <mat-list>\r\n        <mat-list-item>\r\n            <div class=required-container>\r\n                <p mat-line>\r\n                    <mat-form-field>\r\n                        <input matInput  placeholder=\"Name of Issue\" [formControl]=\"nameForm\" [(ngModel)]=\"data.name\" required>\r\n                        <mat-error *ngIf=\"nameForm.invalid\">{{getErrorMessage()}}</mat-error>\r\n                    </mat-form-field>\r\n                </p>\r\n            </div>\r\n        </mat-list-item>\r\n\r\n        <mat-list-item>\r\n            <div class=required-container>\r\n                <p mat-line>\r\n                    <mat-form-field>\r\n                        <mat-select placeholder=\"Severity\" [formControl]=\"severityForm\" [(ngModel)]=\"data.severity\" required>\r\n                            <mat-option>--</mat-option>\r\n                            <mat-option *ngFor=\"let sev of severities\" [value]=\"sev.value\">\r\n                                {{sev.viewValue}}\r\n                            </mat-option>\r\n                        </mat-select>\r\n                        <mat-error *ngIf=\"severityForm.invalid\">{{getErrorMessage()}}</mat-error>\r\n                    </mat-form-field>\r\n                </p>\r\n            </div>\r\n        </mat-list-item>\r\n\r\n        <mat-list-item>\r\n            <p mat-line>\r\n                <mat-form-field>\r\n                    <input matInput placeholder=\"controlBox\" [(ngModel)]=\"data.controlBox\">\r\n                </mat-form-field>\r\n            </p>\r\n        </mat-list-item>\r\n\r\n        <mat-list-item>\r\n            <p mat-line>\r\n                <mat-form-field>\r\n                    <input matInput placeholder=\"controller\" [(ngModel)]=\"data.controller\">\r\n                </mat-form-field>\r\n            </p>\r\n        </mat-list-item>\r\n\r\n        <mat-list-item>\r\n            <p mat-line>\r\n                <mat-form-field>\r\n                    <input matInput placeholder=\"Zone\" [(ngModel)]=\"data.zone\">\r\n                </mat-form-field>\r\n            </p>\r\n        </mat-list-item>\r\n\r\n        <mat-list-item>\r\n            <p mat-line>\r\n                <mat-form-field>\r\n                    <input matInput placeholder=\"Station\" [(ngModel)]=\"data.station\">\r\n                </mat-form-field>\r\n            </p>\r\n        </mat-list-item>\r\n\r\n        <mat-list-item>\r\n            <p mat-line>\r\n                <mat-form-field>\r\n                    <input matInput placeholder=\"Assign To User\"[(ngModel)]=\"data.assignedUser\">\r\n                </mat-form-field>\r\n            </p>\r\n        </mat-list-item>\r\n\r\n        <mat-list-item>\r\n            <p mat-line>\r\n                <mat-form-field>\r\n                    <textarea matInput placeholder=\"Description\" matTextareaAutosize\r\n                    matAutosizeMinRows=\"1\"\r\n                    matAutosizeMaxRows=\"3\" [(ngModel)]=\"data.description\">\r\n                    </textarea>\r\n                </mat-form-field>\r\n        </p>\r\n    </mat-list-item>\r\n\r\n</mat-list>\r\n<div>\r\n    <div mat-dialog-actions>\r\n        <button mat-button (click)=\"onNoClick()\">Cancel</button>\r\n        <button mat-button [mat-dialog-close]=\"data\" [disabled]=\"nameForm.invalid || severityForm.invalid\">Submit</button>\r\n    </div>\r\n"

/***/ }),

/***/ "./src/app/components/notifications/custom-log-dialog-overview.scss":
/***/ (function(module, exports) {

module.exports = ".required-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  width: 350px; }\n\n.required-container > * {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/components/notifications/notifications.component.css":
/***/ (function(module, exports) {

module.exports = ".notifications {\r\n  max-width: auto;\r\n  max-height:500px;\r\n  overflow: auto;\r\n  margin-left: 25px;\r\n  margin-right: 10px;\r\n  margin-bottom: 25px;\r\n}\r\n\r\n#customButton {\r\n  margin-left: 45px;\r\n}\r\n\r\n.right-aligned-header > .mat-content {\r\n  -webkit-box-pack: justify;\r\n      -ms-flex-pack: justify;\r\n          justify-content: space-between;\r\n}\r\n\r\n.mat-content > mat-panel-title, .mat-content > mat-panel-description {\r\n-webkit-box-flex: 0;\r\n    -ms-flex: 0 0 auto;\r\n        flex: 0 0 auto;\r\n}\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/components/notifications/notifications.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n\r\n    <button mat-raised-button color=\"warn\" (click)=\"openCustomIssueDialog()\" id=\"customButton\">Add Custom Issue</button>\r\n\r\n    <mat-card class=\"notifications\" *ngIf=\"authService.loggedIn()\">\r\n        <mat-card-header>\r\n            <mat-card-title>My Notifications</mat-card-title>\r\n        </mat-card-header>\r\n        <mat-card-content>\r\n\r\n            <mat-accordion>\r\n                <mat-expansion-panel (opened)=\"panelOpenState = true\"\r\n                (closed)=\"panelOpenState = false\"\r\n                *ngFor=\"let issue of userIssueLogs | async\">\r\n                <mat-expansion-panel-header class=\"right-aligned-header\">\r\n                    <mat-panel-title>\r\n                        <h5 mat-line>{{ issue.name }}</h5>\r\n                    </mat-panel-title>\r\n                    <mat-panel-description>\r\n                        <mat-chip-list>\r\n                            <mat-chip *ngIf=\"issue.severity=='low'\"color=\"primary\" selected=\"true\">Low</mat-chip>\r\n                            <mat-chip *ngIf=\"issue.severity=='med'\"color=\"accent\" selected=\"true\">Med</mat-chip>\r\n                            <mat-chip *ngIf=\"issue.severity=='high'\"color=\"warn\" selected=\"true\">High</mat-chip>\r\n                        </mat-chip-list>\r\n                    </mat-panel-description>\r\n                </mat-expansion-panel-header>\r\n                <mat-list>\r\n                    <mat-list-item>\r\n                        <p mat-line> Date Created: {{ issue.dateCreated }} </p>\r\n                    </mat-list-item>\r\n\r\n                    <mat-list-item>\r\n                        <p mat-line> Date Completed: {{ issue.dateCompleted }} </p>\r\n                    </mat-list-item>\r\n\r\n                    <mat-list-item>\r\n                        <p mat-line> Assigned To: {{ issue.assignedUser }} </p>\r\n                    </mat-list-item>\r\n\r\n                    <mat-list-item>\r\n                        <p mat-line> Zone: {{ issue.zone }} </p>\r\n                    </mat-list-item>\r\n\r\n                    <mat-list-item>\r\n                        <p mat-item> Control Box: {{ issue.controlBox }} </p>\r\n                    </mat-list-item>\r\n\r\n                    <mat-list-item>\r\n                        <p mat-item> Controller: {{ issue.controller }} </p>\r\n                    </mat-list-item>\r\n\r\n                    <mat-list-item>\r\n                        <p mat-item> Station: {{ issue.station }} </p>\r\n                    </mat-list-item>\r\n\r\n                    <mat-list-item>\r\n                        <p mat-item> Description: {{ issue.description }} </p>\r\n                    </mat-list-item>\r\n\r\n                    <mat-list-item>\r\n                        <p mat-item> <button mat-raised-button\r\n                            color=\"warn\"(click)=\"deleteIssue(issue._id)\"\r\n                            id=\"customButton\">Delete</button> </p>\r\n                        </mat-list-item>\r\n\r\n                        <mat-list-item>\r\n                            <p mat-item> <button mat-raised-button\r\n                                color=\"warn\"(click)=\"openUpdateDialog(issue)\"\r\n                                id=\"customButton\">Edit</button> </p>\r\n                            </mat-list-item>\r\n\r\n                        </mat-list>\r\n                    </mat-expansion-panel>\r\n                </mat-accordion>\r\n            </mat-card-content>\r\n        </mat-card>\r\n\r\n        <mat-card class=\"notifications\" id=\"notificationCard\">\r\n            <mat-card-header>\r\n                <mat-card-title>Other Notifications</mat-card-title>\r\n            </mat-card-header>\r\n            <mat-card-content>\r\n\r\n                <mat-accordion>\r\n                    <mat-expansion-panel (opened)=\"panelOpenState = true\"\r\n                    (closed)=\"panelOpenState = false\"\r\n                    *ngFor=\"let issue of otherIssueLogs | async\">\r\n                    <mat-expansion-panel-header class=\"right-aligned-header\">\r\n                        <mat-panel-title>\r\n                            <h5 mat-line>{{ issue.name }}</h5>\r\n                        </mat-panel-title>\r\n                        <mat-panel-description>\r\n                            <mat-chip-list>\r\n                                <mat-chip *ngIf=\"issue.severity=='low'\"color=\"primary\" selected=\"true\">Low</mat-chip>\r\n                                <mat-chip *ngIf=\"issue.severity=='med'\"color=\"accent\" selected=\"true\">Med</mat-chip>\r\n                                <mat-chip *ngIf=\"issue.severity=='high'\"color=\"warn\" selected=\"true\">High</mat-chip>\r\n                            </mat-chip-list>\r\n                        </mat-panel-description>\r\n                    </mat-expansion-panel-header>\r\n                    <mat-list>\r\n                        <mat-list-item>\r\n                            <p mat-line> Date Created: {{ issue.dateCreated }} </p>\r\n                        </mat-list-item>\r\n\r\n                        <mat-list-item>\r\n                            <p mat-line> Date Completed: {{ issue.dateCompleted }} </p>\r\n                        </mat-list-item>\r\n\r\n                        <mat-list-item>\r\n                            <p mat-line> Assigned To: {{ issue.assignedUser }} </p>\r\n                        </mat-list-item>\r\n\r\n                        <mat-list-item>\r\n                            <p mat-line> Zone: {{ issue.zone }} </p>\r\n                        </mat-list-item>\r\n\r\n                        <mat-list-item>\r\n                            <p mat-item> Control Box: {{ issue.controlBox }} </p>\r\n                        </mat-list-item>\r\n\r\n                        <mat-list-item>\r\n                            <p mat-item> Controller: {{ issue.controller }} </p>\r\n                        </mat-list-item>\r\n\r\n                        <mat-list-item>\r\n                            <p mat-item> Station: {{ issue.station }} </p>\r\n                        </mat-list-item>\r\n\r\n                        <mat-list-item>\r\n                            <p mat-item> Description: {{ issue.description }} </p>\r\n                        </mat-list-item>\r\n\r\n                        <mat-list-item>\r\n                            <button mat-raised-button color=\"warn\"(click)=\"deleteIssue(issue._id)\" id=\"customButton\">Delete</button>\r\n                        </mat-list-item>\r\n                        <mat-list-item>\r\n                            <p mat-item> <button mat-raised-button\r\n                                color=\"warn\"(click)=\"openUpdateDialog(issue)\"\r\n                                id=\"customButton\">Edit</button> </p>\r\n                            </mat-list-item>\r\n                        </mat-list>\r\n                    </mat-expansion-panel>\r\n                </mat-accordion>\r\n            </mat-card-content>\r\n        </mat-card>\r\n    </div>\r\n"

/***/ }),

/***/ "./src/app/components/notifications/notifications.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return NotificationsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomLogDialogOverview; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_validate_service__ = __webpack_require__("./src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_logs_service__ = __webpack_require__("./src/app/services/logs.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_message_service__ = __webpack_require__("./src/app/services/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};








var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(authService, validateService, logService, flashMessage, messageService, dialog) {
        this.authService = authService;
        this.validateService = validateService;
        this.logService = logService;
        this.flashMessage = flashMessage;
        this.messageService = messageService;
        this.dialog = dialog;
        this.panelOpenState = false;
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        this.issueLogs = this.logService.issues;
        if (this.authService.loggedIn()) {
            this.updateUserLogs();
        }
        this.logService.loadAll();
    };
    NotificationsComponent.prototype.submitIssue = function (issue) {
        if (!this.validateService.validateIssueLog(issue)) {
            this.flashMessage.show('Please enter required fields', { cssClass: 'alert-danger', timeout: 5000 });
            return false;
        }
        else {
            this.logService.create(issue);
            this.updateUserLogs();
            this.logService.loadAll(); /*This works for now I guess*/
            this.messageService.sendMessage("Issue Created");
        }
    };
    NotificationsComponent.prototype.updateUserLogs = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (profile) {
            _this.userIssueLogs = _this.issueLogs
                .map(function (issues) { return issues.filter(function (issue) { return (typeof (issue.assignedUser) !== 'undefined'
                && issue.assignedUser === profile.user.name); }); });
            _this.otherIssueLogs = _this.issueLogs
                .map(function (issues) { return issues.filter(function (issue) { return (typeof (issue.assignedUser) === 'undefined'
                || issue.assignedUser !== profile.user.name); }); });
        });
    };
    NotificationsComponent.prototype.deleteIssue = function (issueId) {
        this.logService.remove(issueId);
        this.updateUserLogs();
        this.logService.loadAll();
        this.messageService.sendMessage("Issue Removed");
    };
    NotificationsComponent.prototype.updateIssue = function (issue) {
        this.logService.update(issue);
        this.updateUserLogs();
        this.logService.loadAll();
        this.messageService.sendMessage("Issue Updated");
    };
    NotificationsComponent.prototype.openUpdateDialog = function (issue) {
        var _this = this;
        var dialogRef = this.dialog.open(CustomLogDialogOverview, {
            data: {
                _id: issue._id,
                name: issue.name,
                severity: issue.severity,
                dateCreated: issue.dateCreated,
                dateCompleted: issue.dateCompleted,
                controlBox: issue.controlBox,
                controller: issue.controller,
                zone: issue.zone,
                station: issue.station,
                description: issue.description,
                assignedUser: issue.assignedUser
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (typeof result !== 'undefined' && result != null) {
                _this.updateIssue(result);
            }
        });
    };
    NotificationsComponent.prototype.openCustomIssueDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(CustomLogDialogOverview, {
            data: {
                name: this.name,
                severity: this.severity,
                dateCreated: this.dateCreated,
                dateCompleted: this.dateCompleted,
                controlBox: this.controlBox,
                controller: this.controller,
                zone: this.zone,
                station: this.station,
                description: this.description,
                assignedUser: this.assignedUser
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (typeof result !== 'undefined' && result != null) {
                result.dateCreated = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
                _this.submitIssue(result);
            }
        });
    };
    NotificationsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-notifications',
            template: __webpack_require__("./src/app/components/notifications/notifications.component.html"),
            styles: [__webpack_require__("./src/app/components/notifications/notifications.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__services_validate_service__["a" /* ValidateService */],
            __WEBPACK_IMPORTED_MODULE_6__services_logs_service__["a" /* LogService */],
            __WEBPACK_IMPORTED_MODULE_2_angular2_flash_messages__["FlashMessagesService"],
            __WEBPACK_IMPORTED_MODULE_7__services_message_service__["a" /* MessageService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_material__["c" /* MatDialog */]])
    ], NotificationsComponent);
    return NotificationsComponent;
}());

var CustomLogDialogOverview = /** @class */ (function () {
    function CustomLogDialogOverview(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.severities = [
            { value: 'low', viewValue: 'Low' },
            { value: 'med', viewValue: 'Medium' },
            { value: 'high', viewValue: 'High' }
        ];
        this.nameForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["k" /* Validators */].required]);
        this.severityForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["k" /* Validators */].required]);
    }
    CustomLogDialogOverview.prototype.getErrorMessage = function () {
        if (this.nameForm.hasError('required') ||
            this.severityForm.hasError('required'))
            return 'You must enter a value';
    };
    CustomLogDialogOverview.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    CustomLogDialogOverview = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'custom-log-dialog-overview',
            template: __webpack_require__("./src/app/components/notifications/custom-log-dialog-overview.html"),
            styles: [__webpack_require__("./src/app/components/notifications/custom-log-dialog-overview.scss")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_3__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_material__["d" /* MatDialogRef */], Object])
    ], CustomLogDialogOverview);
    return CustomLogDialogOverview;
}());



/***/ }),

/***/ "./src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<h2 class=\"page-header\">Register</h2>\r\n<form (submit)=\"onRegisterSubmit()\">\r\n    <div class=\"form-group\">\r\n        <label>Name</label>\r\n        <input type=\"text\" [(ngModel)]=\"name\" name=\"name\" class=\"form-control\">\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <label>Username</label>\r\n        <input type=\"text\" [(ngModel)]=\"username\" name=\"username\" class=\"form-control\">\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <label>Email</label>\r\n        <input type=\"text\" [(ngModel)]=\"email\" name=\"email\" class=\"form-control\" >\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <label>Password</label>\r\n        <input type=\"password\" [(ngModel)]=\"password\" name=\"password\" class=\"form-control\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label>Access Level</label>\r\n        <input type=\"text\" [(ngModel)]=\"access\" name=\"access\" class=\"form-control\">\r\n    </div>\r\n    <input type=\"submit\" class=\"btn btn-primary\" value=\"Submit\">\r\n</form>\r\n\r\n"

/***/ }),

/***/ "./src/app/components/register/register.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_validate_service__ = __webpack_require__("./src/app/services/validate.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = /** @class */ (function () {
    //anytime we use a service from another component, we need to inject it into the
    //constructor
    function RegisterComponent(validateService, flashMessage, authService, router) {
        this.validateService = validateService;
        this.flashMessage = flashMessage;
        this.authService = authService;
        this.router = router;
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var user = {
            name: this.name,
            email: this.email,
            access: this.access,
            username: this.username,
            password: this.password
        };
        //Required fields
        if (!this.validateService.validateRegister(user)) {
            this.flashMessage.show('Please enter all fields', { cssClass: 'alert-danger', timeout: 5000 });
            return false;
        }
        //Validate email
        if (!this.validateService.validateEmail(user.email)) {
            this.flashMessage.show('Please enter a valid email', { cssClass: 'alert-danger', timeout: 5000 });
            return false;
        }
        //register user
        this.authService.registerUser(user).subscribe(function (data) {
            if (data.success) {
                _this.flashMessage.show('You are now Registered and can login', { cssClass: 'alert-success', timeout: 5000 });
                _this.router.navigate(['/login']);
            }
            else {
                _this.flashMessage.show('Something went wrong', { cssClass: 'alert-danger', timeout: 5000 });
                _this.router.navigate(['/register']);
            }
        });
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("./src/app/components/register/register.component.html"),
            styles: [__webpack_require__("./src/app/components/register/register.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_validate_service__["a" /* ValidateService */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"],
            __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/components/reports/reports.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/reports/reports.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n    \r\n</div>"

/***/ }),

/***/ "./src/app/components/reports/reports.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ReportsComponent = /** @class */ (function () {
    function ReportsComponent() {
    }
    ReportsComponent.prototype.ngOnInit = function () {
    };
    ReportsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-reports',
            template: __webpack_require__("./src/app/components/reports/reports.component.html"),
            styles: [__webpack_require__("./src/app/components/reports/reports.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ReportsComponent);
    return ReportsComponent;
}());



/***/ }),

/***/ "./src/app/components/roles/roles.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n.container{\r\n    background-color: white;\r\n    width: 550px;\r\n    \r\n}\r\n\r\ninput {\r\n    width: 500px;\r\n}\r\n\r\nselect {\r\n    -webkit-appearance: menulist;    \r\n    width: 500px;\r\n}\r\n\r\nlabel {\r\n    color: black;\r\n}\r\n\r\n#formDiv{\r\n    margin-left: -1px;\r\n}"

/***/ }),

/***/ "./src/app/components/roles/roles.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2\" id=\"formDiv\">\r\n      <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\r\n        <div id=\"user-data\">\r\n           <div class=\"form-group\">\r\n            <label for=\"name\">Name</label>\r\n            <input\r\n               type=\"text\"\r\n               placeholder=\"Enter your full name\"\r\n               id=\"name\"\r\n               class=\"form-control\"\r\n               ngModel\r\n               name=\"name\"\r\n               required>\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <label for=\"username\">Username</label>\r\n            <input\r\n               type=\"text\"\r\n               placeholder=\"Enter your username\"\r\n               id=\"username\"\r\n               class=\"form-control\"\r\n               ngModel\r\n               name=\"username\"\r\n               required>\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <label for=\"email\">Email</label>\r\n            <input\r\n               type=\"email\"\r\n               placeholder=\"Enter your email\"\r\n               id=\"email\"\r\n               class=\"form-control\"\r\n               ngModel\r\n               name=\"email\"\r\n               required\r\n               email>\r\n          </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n        <label for=\"password\">Password</label>\r\n        <input\r\n            type=\"password\"\r\n            placeholder=\"Enter your password\"\r\n            id=\"password\"\r\n            class=\"form-control\"\r\n            ngModel\r\n            name=\"password\"\r\n            required>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <label for=\"access\">Select Access Level</label>\r\n          <select\r\n             id=\"access\"\r\n             class=\"form-control\"\r\n             ngModel\r\n             name=\"access\"\r\n             required>\r\n            <option value=\"\" disabled selected>please choose</option>\r\n            <option value=\"admin\">Admin Level</option>\r\n            <option value=\"user\">User Level</option>\r\n          </select>\r\n        </div>\r\n        <button\r\n           class=\"btn btn-primary\"\r\n           type=\"submit\"\r\n           [disabled]=\"!f.valid\">Submit</button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/roles/roles.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RolesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_message_service__ = __webpack_require__("./src/app/services/message.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RolesComponent = /** @class */ (function () {
    function RolesComponent(authService, messageService) {
        this.authService = authService;
        this.messageService = messageService;
    }
    RolesComponent.prototype.ngOnInit = function () {
    };
    RolesComponent.prototype.onSubmit = function (form) {
        var user = {
            name: form.value.name,
            email: form.value.email,
            access: form.value.access,
            username: form.value.username,
            password: form.value.password
        };
        console.log(user);
        this.authService.registerUser(user)
            .subscribe(function (data) { return console.log(data); });
        form.reset();
        this.messageService.sendMessage("User Created");
    };
    RolesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-roles',
            template: __webpack_require__("./src/app/components/roles/roles.component.html"),
            styles: [__webpack_require__("./src/app/components/roles/roles.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__services_message_service__["a" /* MessageService */]])
    ], RolesComponent);
    return RolesComponent;
}());



/***/ }),

/***/ "./src/app/components/schedule/schedule.component.css":
/***/ (function(module, exports) {

module.exports = "/*Autocomplete */\r\n.example-form {\r\n    background-color: white;\r\n    min-width: 150px;\r\n    max-width: 390px;\r\n    margin-bottom: 15px;\r\n    width: 100%;\r\n  }\r\n/*Autocomplete */\r\n.example-full-width {\r\n    font-size: 18px;\r\n    width: 100%;\r\n    margin-top: 2px;\r\n    margin-bottom: -15px;\r\n  }\r\n.row{\r\n    margin-left: 10px;\r\n  }\r\nlabel{\r\n    color: #3C4858;\r\n    font-weight: 300;\r\n    font-family:  \"Roboto\", \"Helvetica\", \"Arial\", sans-serif;\r\n  }\r\n#amRadio{\r\n    margin-left: 5px;\r\n  }\r\n.availableButton{\r\n    -webkit-user-drag: element;\r\n    -khtml-user-drag: element;\r\n    width: block;\r\n  }\r\n#availableButton:hover{\r\n    padding-top: 15px;\r\n    padding-bottom: 15px;\r\n    font-style: bold;\r\n    -webkit-text-decoration-color: none;\r\n            text-decoration-color: none;\r\n    border-color: rgb(157, 193, 255);\r\n    border-width: 3px;\r\n  }\r\n#droppedButton{\r\n    width: 100%;\r\n    display: block;\r\n    font-size: 12pt;\r\n    font-style: bold;\r\n  }\r\n#droppedButton:hover{\r\n    padding-top: 15px;\r\n    padding-bottom: 15px;\r\n    font-style: bold;\r\n    -webkit-text-decoration-color: none;\r\n            text-decoration-color: none;\r\n    border-style: solid;\r\n    border-width: 3px;\r\n    border-color: rgb(157, 193, 255);\r\n  }\r\n#displayZoneButtons{\r\n    border: none;\r\n  }\r\n#hourInput{\r\n    width: 50px;\r\n  }\r\n#minuteInput{\r\n    width: 75px;\r\n  }\r\n#nameLabel{\r\n    float: left;\r\n    margin-left: 15px;\r\n  }\r\n#offSetInput{\r\n    width: 25%;\r\n    margin-left: 4em;\r\n  }\r\n#runTime{\r\n    width: 25%;\r\n    float: right;\r\n  }\r\n#runTimeLabel{\r\n    float: right;\r\n    margin-right: 15px;\r\n  }\r\n#zone-summary{\r\n    /*background-color: rgb(196, 196, 196);*/\r\n    height: auto;\r\n    width: 390px;\r\n    text-align: center;\r\n  }\r\n#stationList{\r\n    background-color: rgb(255, 255, 255);\r\n    width: 250px;\r\n    text-align: center;\r\n  }\r\n#scheduleList{\r\n    background-color: rgb(255, 255, 255);\r\n    border-bottom: medium;\r\n    border-bottom-color: rgb(238,238,238);\r\n    width: 400px;\r\n    text-align: center;\r\n  }\r\n#scheduleListTitle{\r\n    border-style: none;\r\n  }\r\n#stationDescription{\r\n    border: visible;\r\n    border-top-style: solid;\r\n    border-top-width: 5px;\r\n    border-top-color: rgb(238,238,238);\r\n    font-size: 14pt;\r\n    text-align: center;\r\n  }\r\n#saveButton{\r\n    text-align: center;\r\n    block-size: auto;\r\n  }\r\n#saveScheduleButton{\r\n    -ms-flex-item-align: auto;\r\n        -ms-grid-row-align: auto;\r\n        align-self: auto; \r\n  }\r\n#savedSchedule{\r\n    background-color: rgb(255, 255, 255);\r\n    height: auto;\r\n    width: 455px;\r\n    text-align: center;\r\n  }\r\n.scheduleLabels{\r\n    text-indent: 1em;\r\n    border-top-width: 2px;\r\n    border-top-style: solid;\r\n    border-top-color: rgb(238,238,238);\r\n    text-align: left;\r\n    display: block;\r\n  }\r\n.scheduleListLabels{\r\n    text-indent: 10em;\r\n  }\r\n:host ::ng-deep .cycleDay{\r\n    text-decoration: none;\r\n    padding: 5px;\r\n    color: #3C4858;\r\n    font-weight: 300;\r\n    font-family: \"Roboto\", \"Helvetica\", \"Arial\", sans-serif;\r\n  }\r\n.cycleLabels{\r\n    padding-top: 5px;\r\n    padding-right: 1em;\r\n  }\r\n#startTimeForm {\r\n    text-align: center;\r\n    padding-left: 25px;\r\n  }\r\n.currentScheduleLabels {\r\n    padding-right: 100px;\r\n    padding-left: 1em;\r\n    text-align: left;\r\n    float: left;\r\n  }\r\n#currentScheduleBlock {\r\n    border-top-width: 2px;\r\n    border-top-style: solid;\r\n    border-top-color: rgb(238,238,238);\r\n    margin-bottom: -2px;\r\n  }\r\n#viewScheduleButtons:hover{\r\n      border-width: 3px;\r\n      border-color: rgb(157, 193, 255);\r\n      padding-top: 15px;\r\n      padding-right: 0px;\r\n      padding-left: 0px;\r\n      padding-bottom: 15px;\r\n  }\r\n.viewButtonLabels{\r\n    float: left;\r\n    padding-right: 100px;\r\n  }\r\n#viewButtonLabelTimer{\r\n    padding-left: 0px;\r\n    float: right;\r\n    padding-right: 0px;\r\n  }\r\n#viewScheduleButtons{\r\n    text-decoration: none;\r\n  }\r\n/*crd zone-summary text */\r\n.mdc-card__supporting-text{\r\n    text-align: center;\r\n    /*padding-bottom: 10px;*/\r\n  }\r\n.mdc-card{\r\n    text-align: center;\r\n    border: hidden;\r\n    height: auto;\r\n    background: white;\r\n  }\r\n.mdc-card__primary{\r\n    border-bottom: solid;\r\n    border-bottom-color: rgb(212, 211, 211);\r\n    margin-top: -15px;\r\n    height: auto;\r\n  }\r\n.mdc-card__actions{\r\n    padding-bottom: 10px;\r\n  }\r\nimg {\r\n    height: 300px;\r\n    width: 100%;\r\n    padding-top: 5px;\r\n    padding-left: 5px;\r\n    padding-right:5px;\r\n  }\r\n.list-group {\r\n     max-height: 300px;\r\n     overflow-y: auto;\r\n }\r\n#cycleSection{\r\n   text-align: left;\r\n   margin-left: 10px;\r\n   font-size: 10pt;\r\n   padding-bottom: 10px;\r\n }\r\n#timeSection{\r\n   font-size: 12pt;\r\n   font-weight: 700;\r\n }\r\n"

/***/ }),

/***/ "./src/app/components/schedule/schedule.component.html":
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html>\r\n<div class=\"main-content\">\r\n  <html>\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-6\">\r\n      <form class=\"example-form\">\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input type=\"text\" placeholder=\"Pick a zone\" aria-label=\"Number\" matInput [formControl]=\"myControl\" [matAutocomplete]=\"auto\">\r\n          <mat-autocomplete #auto=\"matAutocomplete\">\r\n            <mat-option *ngFor=\"let zone of irrigationZones | async\" [value]=\"option\" (click)=\"onZoneClick(zone)\">\r\n              {{ zone.name }}\r\n            </mat-option>\r\n          </mat-autocomplete>\r\n        </mat-form-field>\r\n      </form>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" *ngIf=\"displayZoneCard\">\r\n\r\n    <div *ngIf=\"displayZoneSummary;else display_stationList\">\r\n      <div class=\"col-xs-4\">\r\n        <div class=\"mdc-card\" id=\"zone-summary\">\r\n          <section class=\"mdc-card-media\">\r\n            <img src=\"{{ setZoneImage(zoneName) }}\" alt=\"no image for {{ zoneName }}\">\r\n          </section>\r\n          <section class=\"mdc-card__primary\">\r\n            <h2 class=\"mdc-card__subtitle\">{{ zoneName }}</h2>\r\n          </section>\r\n          <section class=\"mdc-card__supporting-text\">\r\n            {{ zoneDescription }}\r\n          </section>\r\n          <section class=\"mdc-card__actions\" id=\"displayZoneButtons\">\r\n            <button [disabled]=\"isSchedules()\" class=\"btn btn-danger\" (click)=\"onViewScheduleClick()\">\r\n              View Schedules\r\n            </button>\r\n            <button class=\"btn btn-primary\" (click)=\"onCreateScheduleClick()\">\r\n              Create a Schedule\r\n            </button>\r\n          </section>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <ng-template #display_stationList>\r\n      <div *ngIf=\"createSchedule;else display_Schedule\">\r\n         <hr>\r\n        <div class=\"col-xs-4\">\r\n          <div class=\"mdc-card\" id=\"stationList\">\r\n            <section class=\"mdc-card__primary\" id=\"stationListTitle\">\r\n               <img src=\"{{ setArcGisImage(zoneSelected.name) }}\" alt=\"No arcGIS image for {{ zoneSelected.name }}\">\r\n              <h2 class=\"mdc-card__subtitle\">Available Stations</h2>\r\n            </section>\r\n            <div class=\"container\" class=\"list-group\" [dragula]='\"first-bag\"' [dragulaModel]='stations'>\r\n              <div *ngFor='let station of zoneSelected.stations' ng-draggable=\"true\">\r\n                <button type=\"button\" class=\"list-group-item list-group-item-{{ setColorStation(station.controllerId) }}\" (click)=\"onStationClick(station.name, station.controller)\" id=\"availableButton\">\r\n                  <label> {{ station.name }} </label>\r\n                </button>\r\n              </div>\r\n            </div>\r\n            <section class=\"mdc-card__subtitle\" *ngIf=\"displayStationGeneral\">\r\n                    <h3>Station Info </h3>\r\n                    <p id=\"stationDescription\">\r\n                      I'm going to put this stuff in the javascript so it is easier and reliable.\r\n                    </p>\r\n            </section>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-xs-5\">\r\n\t\t<form ngNativeValidate id=\"startTimeForm\">\r\n          <div class=\"mdc-card\" id=\"scheduleList\">\r\n            <section class=\"mdc-card__primary\" id=\"scheduleListTitle\">\r\n              <section class=\"mdc-card__subtitle\">\r\n                <h2>New Schedule</h2>\r\n              </section>\r\n\r\n                  <label>Start Time\r\n                    <input type=\"number\" name=\"hour\" \tplaceholder=\"Hour\" \t\tid=\"hourInput\" \t\tmax=\"12\" \tmin=\"0\" required>:\r\n                    <input type=\"number\" name=\"minute\" \tplaceholder=\"minute\" \tid=\"minuteInput\" \tmax=\"59\"\tmin=\"0\" required>\r\n                  </label>\r\n                  <label> <input type=\"radio\" name=\"timeofDay\" value=\"a.m.\" id=\"amRadio\" required> a.m.</label>\r\n                  <label> <input type=\"radio\" name=\"timeofDay\" value=\"p.m\"> p.m.</label>\r\n                <!--/form-->\r\n                <p>\r\n                  <label class =\"cycleLabels\">weekly <input type=\"radio\" name=\"cycle\" value=\"1\" (click)=\"getCycle(1)\" required></label>\r\n                  <label class =\"cycleLabels\">bi-weekly <input type=\"radio\" name=\"cycle\" value=\"2\" (click)=\"getCycle(2)\"></label>\r\n\t\t\t\t          <label>monthly <input type=\"radio\" name=\"cycle\" value=\"4\" (click)=\"getCycle(4)\"></label>\r\n\t\t\t\t        </p>\r\n\t\t\t\t<section id='scheduleDaysID'>\r\n\t\t\t\t\t<!-- Injected Dynamically through getCycle() in Javascript\r\n\t\t\t\t\t\t Set by weekly, bi-weekly and monthly (1,2 or 4)\t-->\r\n\t\t\t\t  </section>\r\n\r\n                  <p class =\"scheduleLabels\">\r\n\t\t\t\t\t\t<span><label>Name</label></span>\r\n\t\t\t\t  \t\t<span class=\"scheduleListLabels\"><label>Overlap</label></span>\r\n\t\t\t\t\t\t<span class=\"scheduleListLabels\"><label>Run Time</label></span>\r\n\t\t\t\t  </p>\r\n            </section>\r\n            <section class=\"mdc-card__supporting-text\">\r\n            <div class=\"container\" class=\"list-group\" id='no-drop' [dragula]='\"first-bag\"' [dragulaModel]='scheduleList' draggable=\"true\">\r\n              <div *ngIf=\"!scheduleList.length\">Drag Stations here to create Schedule</div>\r\n              <div *ngFor='let station of scheduleList; let i = index;'>\r\n            \t<div>\r\n                  <popover-content #myPopover title=\"Controller: {{ station.controller }}, Station: {{ station.name }}\" placement=\"above\" [closeOnMouseOutside]=\"true\">\r\n                    <button type=\"button\" class=\"btn btn-danger\" (click)=\"onDeleteStationClick(i)\">Delete</button>\r\n                  </popover-content>\r\n                </div>\r\n                <div>\r\n                  <section id=\"timerForm\">\r\n                    <button type=\"button\" [popover]=\"myPopover\" class=\"list-group-item list-group-item-{{ station.controllerColor }}\" id=\"droppedButton\">\r\n                      <label>{{ station.name }} </label>\r\n                        <input type=\"number\" class=\"offsetTime\" name=\"offset\" placeholder=\"min\" min=\"0\" id=\"offSetInput\">\r\n                        <input type=\"number\" class=\"timer\" name=\"run\" placeholder=\"min\" min=\"0\" id=\"runTime\" required>\r\n                    </button>\r\n\t\t\t\t</section>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </section>\r\n            <section class=\"mdc-card__actions\" id=\"saveScheduleButton\">\r\n              <button type=\"sumbit\" class=\"btn btn-primary\" (click)=\"onSaveScheduleClick()\">\r\n                Save Schedule\r\n              </button>\r\n\t\t\t</section>\r\n\t\t\t</div>\r\n\t\t</form>\r\n\t\t</div>\r\n\t\t</div>\r\n    </ng-template>\r\n\r\n    <ng-template #display_Schedule>\r\n      <div class=\"col-xs-4\" (load)=\"printCycleDays()\">\r\n        <div class=\"mdc-card\" id=\"savedSchedule\">\r\n          <section class=\"mdc-card__primary\">\r\n            <h2 class=\"mdc-card__subtitle\" id=\"savedScheduleTitle\">\r\n                Current  Schedule\r\n            </h2>\r\n            <!--ng-template [ngIf]=\"!isSchedules()\"-->\r\n            <p id=\"timeSection\">Start time: {{ zoneSelected.schedules[0].startTime }}</p>\r\n              <section *ngFor='let weeks of viewCycleSchedule' id=\"cycleSection\">\r\n                {{ weeks }}\r\n              </section>\r\n          <!--/ng-template-->\r\n\r\n            <p id=\"currentScheduleBlock\">\r\n              <label class=\"currentScheduleLabels\">Name</label>\r\n              <label class=\"currentScheduleLabels\">OverLap</label>\r\n              <label>Timer</label>\r\n            </p>\r\n          </section>\r\n          <div class=\"container\" class=\"list-group\">\r\n            <div *ngIf=\"isSchedules()\">No set Schedule for this Zone!</div>\r\n            <!--ng-template [ngIf]=\"!isSchedules()\"-->\r\n            <div *ngFor='let station of zoneSelected.schedules[0].stations'>\r\n              <button type=\"button\" class=\"list-group-item list-group-item-{{ station.controllerColor }}\" id=\"viewScheduleButtons\">\r\n                <label class=\"viewButtonLabels\"> {{ station.name }}</label>\r\n                <label class=\"viewButtonLabels\">{{ station.overlap }}min</label>\r\n                <label id=\"viewButtonLabelTimer\">{{ station.runTime }}min </label>\r\n              </button>\r\n            </div>\r\n        <!--/ng-template-->\r\n\r\n          </div>\r\n          <section class=\"mdc-card__subtitles\">\r\n            <p>\r\n              <label class=\"availableBottom\">Total Run Time: </label>\r\n            </p>\r\n            <p>\r\n              <label class =\"availableBottom\">End Time: </label>\r\n            </p>\r\n          </section>\r\n        </div>\r\n      </div>\r\n    </ng-template>\r\n  </div>\r\n  </html>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/schedule/schedule.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScheduleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators_startWith__ = __webpack_require__("./node_modules/rxjs/_esm5/operators/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators_map__ = __webpack_require__("./node_modules/rxjs/_esm5/operators/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_dragula_ng2_dragula__ = __webpack_require__("./node_modules/ng2-dragula/ng2-dragula.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_dragula_ng2_dragula___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_dragula_ng2_dragula__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_irrigationZones_service__ = __webpack_require__("./src/app/services/irrigationZones.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import 'rxjs/add/operator/startWith';
//import 'rxjs/add/operator/map';




var ScheduleComponent = /** @class */ (function () {
    function ScheduleComponent(DragulaService, irrigationZoneService) {
        this.DragulaService = DragulaService;
        this.irrigationZoneService = irrigationZoneService;
        this.displayZoneSummary = true;
        this.displayZoneCard = false;
        this.createSchedule = false;
        this.displayStationGeneral = false;
        this.displayCurrentSchedule = false;
        this.viewCycleSchedule = [];
        this.stations = [];
        this.scheduleList = [];
        this.irrigationZonesArray = [];
        this.myControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormControl */](); //for autocomplete
        this.options = []; //options for autocomplete
        DragulaService.setOptions('first-bag', {
            copy: function (el, source) {
                if (source.id != 'no-drop') {
                    return true;
                }
                else {
                    return false;
                }
            },
            accepts: function (el, target, source, sibling) {
                if (target.id == 'no-drop') {
                    return true;
                }
                else {
                    return false;
                }
            }
        });
    }
    ScheduleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.irrigationZones = this.irrigationZoneService.irrigationZones;
        this.irrigationZoneService.loadAll();
        this.filteredOptions = this.myControl.valueChanges
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators_startWith__["a" /* startWith */])(''), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators_map__["a" /* map */])(function (val) { return _this.filter(val); }));
    };
    ScheduleComponent.prototype.ngOnDestroy = function () {
        this.DragulaService.destroy('first-bag');
    };
    ScheduleComponent.prototype.filter = function (val) {
        return this.options.filter(function (option) {
            return option.toLowerCase().indexOf(val.toLowerCase()) === 0;
        });
    };
    ScheduleComponent.prototype.onStationClick = function (station, controller) {
        if (this.displayStationGeneral === false)
            this.displayStationGeneral = true;
        this.stationNumber = station;
        this.controllerNumber = controller;
    };
    ScheduleComponent.prototype.onCreateScheduleClick = function (station, controller) {
        this.displayZoneSummary = false;
        this.createSchedule = true;
        this.scheduleList = [];
    };
    ScheduleComponent.prototype.onZoneClick = function (zone) {
        this.zoneSelected = zone;
        this.zoneName = zone.name;
        this.zoneDescription = zone.description;
        this.stations = zone.stations;
        this.displayZoneCard = true;
        this.displayStationGeneral = false;
        if (!this.displayZoneSummary) {
            this.displayZoneSummary = true;
            this.createSchedule = false;
        }
    };
    ScheduleComponent.prototype.onSaveScheduleClick = function () {
        if (this.scheduleList.length != 0) {
            var scheduleName = window.prompt("Name the schedule!");
            var startHour = document.getElementById('startTimeForm')[0].value; //hour
            var startMin = document.getElementById('startTimeForm')[1].value; //minute
            var timeOfDay = document.getElementById('startTimeForm')[2]; //am or pm.m
            if (timeOfDay.checked === true) {
                timeOfDay = timeOfDay.value; //a.m.
            }
            else {
                timeOfDay = document.getElementById('startTimeForm')[3].value; //p.m.
            }
            for (var i = 0; i < this.scheduleList.length; i++) {
                //set Timers for schedule
                var position = i + "";
                this.scheduleList[i].runTime = document.getElementsByName('run')[position].value;
                this.scheduleList[i].overlap = document.getElementsByName('offset')[position].value;
            }
            var schedule = {
                name: scheduleName,
                startTime: JSON.stringify(startHour + ':' + startMin + timeOfDay),
                cycle: JSON.stringify(this.saveCycle()),
                cycleLength: this.theScheduleLength,
                stations: this.scheduleList
            }; //check to see if schedule isn't empty.
            //saves the cycle to zon
            this.zoneSelected.schedules.push(schedule);
        }
        this.irrigationZoneService.update(this.zoneSelected);
        this.irrigationZoneService.loadAll();
    };
    /* save cycle(weekly, bi-weekly, monthly) and saves what days it
    saves for the cycle period.*/
    ScheduleComponent.prototype.saveCycle = function () {
        var cycleSchedule = [];
        var cyclelength;
        for (var i = 0; i < document.getElementsByName('cycle').length; i++) { //this gets the radiobutton check for (weekly, biweekly, monthly)
            var position = i + "";
            var cycle = document.getElementsByName('cycle')[position];
            if (cycle.checked)
                cyclelength = Number(cycle.value); //1,2,4
        }
        var userInput = document.getElementsByName('day');
        for (var j = 0; j < userInput.length; j++) { //goes through all inputs even if unchecked.
            var position = j + "";
            if (userInput[position].checked) {
                cycleSchedule[j] = userInput[position].value;
            }
            else {
                cycleSchedule[j] = "-1"; //did not get checked.
            }
        }
        this.theScheduleLength = cyclelength;
        return cycleSchedule;
    };
    ScheduleComponent.prototype.printCycleDays = function () {
        if (this.zoneSelected.schedules != null) {
            var schedule = this.zoneSelected.schedules[0]; //just having it print the first schedule
            var cycleSchedule = JSON.parse(schedule.cycle);
            //var cycleSchedule = this.zoneSelected.getCycleSchedule();
            //let cycleLength = cycleSchedule.length; //this.zoneSelected.getCycleScheduleLength();
            var cycleLength = this.zoneSelected.schedules[0].cycleLength;
            var outputArray = [];
            var i = 0;
            if (cycleSchedule != null && cycleLength != null) {
                while (i < cycleLength) {
                    outputArray[i] = "Week " + (i + 1) + ": ";
                    for (var j = i * 7; j < (7 * (i + 1)); j++) {
                        if (cycleSchedule[j] != "-1")
                            outputArray[i] += cycleSchedule[j] + " ";
                    }
                    i++;
                }
            }
        }
        return outputArray;
    };
    ScheduleComponent.prototype.onViewScheduleClick = function () {
        this.displayZoneSummary = false;
        this.createSchedule = false;
        this.displayCurrentSchedule = true;
        this.displayZoneCard = true;
        this.viewCycleSchedule = this.printCycleDays(); //for output
        console.log(this.viewCycleSchedule);
    };
    ScheduleComponent.prototype.getCycle = function (cycleType) {
        document.getElementById('scheduleDaysID').innerHTML = ("");
        for (var i = 0; i < cycleType; i++) {
            var cycleDays = ("<section><label class='cycleDay'>Week " + (i + 1) + ": </label>" +
                "<label class='cycleDay'>Sun<input type='checkbox' value='Sunday' name='day'></label>" +
                "<label class='cycleDay'>Mon<input type='checkbox' value='Monday' name='day'></label>" +
                "<label class='cycleDay'>Tues<input type='checkbox' value='Tuesday' name='day'></label>" +
                "<label class='cycleDay'>Wed<input type='checkbox' value='Wednesday' name='day'></label>" +
                "<label class='cycleDay'>Thur<input type='checkbox' value='Thursday' name='day'></label>" +
                "<label class='cycleDay'>Fri<input type='checkbox' value='Friday' name='day'></label>" +
                "<label class='cycleDay'>Sat<input type='checkbox' value='Saturday' name='day'></label></section>");
            document.getElementById('scheduleDaysID').insertAdjacentHTML('beforeend', cycleDays);
        }
    };
    ScheduleComponent.prototype.isSchedules = function () {
        return !Array.isArray(this.zoneSelected.schedules) || !this.zoneSelected.schedules.length;
    };
    ScheduleComponent.prototype.onDeleteStationClick = function (index) {
        this.scheduleList.splice(index, 1);
    };
    ScheduleComponent.prototype.setColorStation = function (controllerId) {
        if (controllerId.includes("controller0")) {
            return "info";
        }
        else if (controllerId.includes("controller1")) {
            return "warning";
        }
        else if (controllerId.includes("controller2")) {
            return "success";
        }
    };
    ScheduleComponent.prototype.setArcGisImage = function (zoneName) {
        if (zoneName.toLowerCase().includes("oviatt")) {
            return "https://i.imgur.com/AvO2ZQV.jpg";
        }
        else {
            return "";
        }
    };
    ScheduleComponent.prototype.setZoneImage = function (zoneName) {
        if (zoneName.toLowerCase().includes("oviatt")) {
            return "http://library.csun.edu/blogs/cited/wp-content/uploads/sites/4/2016/08/Oviatt-Library.jpg";
        }
    };
    ScheduleComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'schedule-component',
            template: __webpack_require__("./src/app/components/schedule/schedule.component.html"),
            styles: [__webpack_require__("./src/app/components/schedule/schedule.component.css")],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ng2_dragula_ng2_dragula__["DragulaService"],
            __WEBPACK_IMPORTED_MODULE_5__services_irrigationZones_service__["a" /* IrrigationZoneService */]])
    ], ScheduleComponent);
    return ScheduleComponent;
}());



/***/ }),

/***/ "./src/app/components/sidebar/sidebar.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"logo\">\r\n    <a class=\"simple-text\">\r\n        <div class=\"logo-img\">\r\n            <img src=\"/assets/img/angular2-logo-red.png\"/>\r\n        </div>\r\n        Umbrella Irrigation\r\n    </a>\r\n</div>\r\n<div class=\"sidebar-wrapper\">\r\n    <form class=\"navbar-form navbar-right\" role=\"search\" *ngIf=\"isMobileMenu()\">\r\n        <div class=\"form-group form-black is-empty\">\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\r\n            <span class=\"material-input\"></span>\r\n        </div>\r\n        <button type=\"submit\" class=\"btn btn-white btn-round btn-just-icon\">\r\n            <i class=\"material-icons\">search</i><div class=\"ripple-container\"></div>\r\n        </button>\r\n    </form>\r\n\r\n    <div class=\"nav-container\">\r\n        <ul class=\"nav\">\r\n            <li routerLinkActive=\"active\" *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}}\">\r\n                <a  [routerLink]=\"[menuItem.path]\">\r\n                    <i class=\"material-icons\">{{menuItem.icon}}</i>\r\n                    <p>{{menuItem.title}}</p>\r\n                </a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ROUTES = [
    { path: 'notifications', title: 'Notifications', icon: 'notifications', class: '' },
    { path: 'schedule', title: 'Schedule', icon: 'content_paste', class: '' },
    { path: 'weather', title: 'Weather', icon: 'cloud', class: '' },
    { path: 'events', title: 'Events', icon: 'bookmark_border', class: '' },
    { path: 'maps', title: 'Map', icon: 'location_on', class: '' },
    { path: 'man-operations', title: 'Manual Operations', icon: 'settings', class: '' },
    { path: 'reports', title: 'Reports', icon: 'assessment', class: '' },
    { path: 'roles', title: 'Roles', icon: 'people', class: '' },
    { path: 'irrigation-zones', title: 'Manage Zones', icon: 'extension', class: '' }
];
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent() {
    }
    SidebarComponent.prototype.ngOnInit = function () {
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
    };
    SidebarComponent.prototype.isMobileMenu = function () {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    ;
    SidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__("./src/app/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__("./src/app/components/sidebar/sidebar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/components/splashPage/splashPage.component.css":
/***/ (function(module, exports) {

module.exports = ".bg {\r\n   background-color: white;\r\n   background-size: cover;\r\n   background-position: center;\r\n   font-family: Oswald;\r\n   height: 100vh;\r\n}\r\n\r\nlabel {\r\n   color: black;\r\n}\r\n\r\nhtml {\r\n   height: 100%;\r\n}\r\n\r\n#content {\r\n   text-align: center;\r\n   padding-top: 15%;\r\n   /*text-shadow: 0px 4px 3px rgba(0, 0, 0, 0.4),\r\n                0px 8px 13px rgba(0, 0, 0, 0.1),\r\n                0px 18px 23px rgba(0, 0, 0, 0.1);*/\r\n}\r\n\r\n.form-group {\r\n    display: block;\r\n    width: 50%;\r\n    margin: auto;\r\n    height: 34px;\r\n    padding: 6px 12px;\r\n    font-size: 25px;\r\n    line-height: 1.42857143;\r\n    color: #555;\r\n    background-color: #fff;\r\n    background-image: none;\r\n    border: 1px solid #ccc;\r\n    border-radius: 4px;\r\n    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);\r\n    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);\r\n    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;\r\n    transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;\r\n    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;\r\n    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;\r\n}\r\n\r\nh1 {\r\n   font-weight: 700;\r\n   font-size: 5em;\r\n}\r\n\r\nhr {\r\n   width: 400px;\r\n   border-top: 1px solid #f8f8f8;\r\n   border-bottom: 1px solid rgba(0, 0, 0, 0.2);\r\n}\r\n\r\n.required-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n}\r\n\r\n.input-form {\r\n    background-color: white;\r\n    font-size: 36px;\r\n}\r\n\r\n.mat-error {\r\n    font-size: 12px;\r\n}\r\n\r\n.required-container > * {\r\n    width: 100%;\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/components/splashPage/splashPage.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"bg\">\r\n   <nav class=\"navbar navbar-default\">\r\n            <div class=\"container\">\r\n               <!-- Brand and toggle get grouped for better mobile display -->\r\n               <div class=\"navbar-header\">\r\n                  <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\r\n                     <span class=\"sr-only\">Toggle navigation</span>\r\n                     <span class=\"icon-bar\"></span>\r\n                     <span class=\"icon-bar\"></span>\r\n                     <span class=\"icon-bar\"></span>\r\n                  </button>\r\n                  <a class=\"navbar-brand\" href=\"#\"><i class=\"fa fa-umbrella\"></i> Umbrella Irrigation</a>\r\n               </div>\r\n\r\n               <!-- Collect the nav links, forms, and other content for toggling -->\r\n               <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n                  <ul class=\"nav navbar-nav\">\r\n                     <li><a href=\"#\">Contact</a></li>\r\n                  </ul>\r\n               </div><!-- /.navbar-collapse -->\r\n            </div><!-- /.container-fluid -->\r\n         </nav>\r\n\r\n         <div class=\"container\">\r\n            <div class=\"row\">\r\n               <div class=\"col-lg-12\">\r\n                  <div id=\"content\">\r\n                     <h1>Umbrella Irrigation</h1>\r\n                     <h3>Serving All Your Irrigation Needs</h3>\r\n                     <hr>\r\n\r\n                         \r\n                    <div class=required-container>\r\n                        <p mat-line>\r\n                        <mat-form-field>\r\n                            <input matInput placeholder=\"username\"\r\n                            class=\"input-form\"\r\n                            [formControl]=\"userForm\" [(ngModel)]=\"username\">\r\n                            <mat-error\r\n                                *ngIf=\"userForm.invalid\">{{getErrorMessage()}}\r\n                            </mat-error>\r\n                        </mat-form-field>\r\n                        </p>\r\n                    </div>\r\n\r\n                    <div class=required-container>\r\n                        <p mat-line>\r\n                        <mat-form-field>\r\n                            <input matInput  placeholder=\"password\"\r\n                            class=\"input-form\"\r\n                            type=\"password\"\r\n                            [formControl]=\"passwordForm\" [(ngModel)]=\"password\">\r\n                            <mat-error\r\n                                *ngIf=\"userForm.invalid\">{{getErrorMessage()}}\r\n                            </mat-error>\r\n                        </mat-form-field>\r\n                        </p>\r\n                    </div>\r\n\r\n                     <!--\r\n                     <form>\r\n                    <div class=\"required-container\">\r\n                        <div class=\"form-group\">\r\n                           <label for=\"username\">Username</label>\r\n                           <input matInput \r\n                                [formControl]=\"userForm\"\r\n                              type=\"text\"\r\n                              class=\"form-control\"\r\n                              id=\"username\"\r\n                              placeholder=\"Username\"\r\n                              [(ngModel)]=\"username\"\r\n                              name=\"username\">\r\n                           <mat-error *ngIf=\"userForm.Invalid\"> {{getErrorMessage()}}</mat-error>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"required-container\">\r\n                        <div class=\"form-group\">\r\n                           <label for=\"exampleInputPassword1\">Password</label>\r\n                           <input matInput\r\n                                [formControl]=\"passwordForm\"\r\n                              type=\"password\"\r\n                              class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"Password\"\r\n                              [(ngModel)]=\"password\"\r\n                              name=\"password\">\r\n                        </div>\r\n                    </div> -->\r\n                        <button\r\n                           (click)=\"onClick()\"\r\n                           type=\"submit\"\r\n                           class=\"btn btn-default btn-lg\"\r\n                           value=\"Login\"><i class=\"fa fa-tint\"></i>  Login</button>\r\n                     <!-- <button class=\"btn btn-default btn-lg\"><i class=\"fas fa-tint\"></i> Sign In</button> -->\r\n                  </div>\r\n               </div>\r\n            </div>\r\n         </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/splashPage/splashPage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__ = __webpack_require__("./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SplashPageComponent = /** @class */ (function () {
    function SplashPageComponent(authService, router, flashMessageService) {
        this.authService = authService;
        this.router = router;
        this.flashMessageService = flashMessageService;
        this.userForm = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["k" /* Validators */].required]);
        this.passwordForm = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["k" /* Validators */].required]);
    }
    SplashPageComponent.prototype.ngOnInit = function () {
    };
    SplashPageComponent.prototype.onClick = function () {
        var _this = this;
        var user = {
            username: this.username,
            password: this.password
        }; //submit through the auth service to the backend authentication route
        this.authService.authenticateUser(user).subscribe(function (data) {
            if (data.success) {
                _this.authService.storeUserData(data.token, data.user);
                //this.flashMessageService.show('Login Success!', {cssClass: 'alert-success', timeout:5000, closeOnClick:true});
                _this.router.navigate(['notifications']);
            }
            else {
                //this.flashMessageService.show(data.msg, {cssClass: 'alert-danger', timeout:5000, closeOnClick:true});
                _this.router.navigate(['login']);
            }
        });
    };
    SplashPageComponent.prototype.getErrorMessage = function () {
        if (this.userForm.hasError('required') ||
            this.passwordForm.hasError('required'))
            return 'You must enter a value';
    };
    SplashPageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-splashPage',
            template: __webpack_require__("./src/app/components/splashPage/splashPage.component.html"),
            styles: [__webpack_require__("./src/app/components/splashPage/splashPage.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_flash_messages__["FlashMessagesService"]])
    ], SplashPageComponent);
    return SplashPageComponent;
}());



/***/ }),

/***/ "./src/app/components/weather/weather.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/weather/weather.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n    \r\n</div>"

/***/ }),

/***/ "./src/app/components/weather/weather.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeatherComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WeatherComponent = /** @class */ (function () {
    function WeatherComponent() {
    }
    WeatherComponent.prototype.ngOnInit = function () {
    };
    WeatherComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-weather',
            template: __webpack_require__("./src/app/components/weather/weather.component.html"),
            styles: [__webpack_require__("./src/app/components/weather/weather.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WeatherComponent);
    return WeatherComponent;
}());



/***/ }),

/***/ "./src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__("./src/app/services/auth.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.authService.loggedIn()) {
            return true; //logged in
        }
        else {
            this.router.navigate(['login']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__ = __webpack_require__("./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
    }
    /*
    Look at the users.js in the routes directory. We make the POST request to register
    It then takes all the form fields and maps it to the newUser object, which is then
    ran through the addUser()
    */
    AuthService.prototype.registerUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('http://localhost:3000/users/register', user, { headers: headers })
            .map(function (response) { return response.json(); });
    };
    AuthService.prototype.authenticateUser = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('http://localhost:3000/users/authenticate', user, { headers: headers })
            .map(function (response) { return response.json(); });
    };
    AuthService.prototype.getProfile = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('Content-Type', 'application/json');
        return this.http.get('http://localhost:3000/users/profile', { headers: headers })
            .map(function (response) { return response.json(); });
    };
    AuthService.prototype.storeUserData = function (token, user) {
        localStorage.setItem('id_token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
        console.log('asdfasfasfasdfasdfasdfasd' + this.user);
    };
    AuthService.prototype.loadToken = function () {
        var token = localStorage.getItem('id_token');
        this.authToken = token;
    };
    AuthService.prototype.loggedIn = function () {
        return Object(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["tokenNotExpired"])('id_token');
    };
    AuthService.prototype.logout = function () {
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    };
    AuthService.prototype.isAdmin = function (user) {
        return user.access === 'admin';
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/controlBoxes.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ControlBoxService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ControlBoxService = /** @class */ (function () {
    function ControlBoxService(http) {
        this.http = http;
        this.dataStore = { stations: [] };
        this.allstations = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]([]);
        this.stations = this.allstations.asObservable();
    }
    ControlBoxService.prototype.loadAllStations = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        this.http.get('http://localhost:3000/data/stations', { headers: headers })
            .subscribe(function (data) {
            _this.dataStore.stations = data.json();
            _this.allstations.next(Object.assign({}, _this.dataStore).stations);
        }, function (error) { return console.log('Could not Load Stations'); });
    };
    ControlBoxService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], ControlBoxService);
    return ControlBoxService;
}());



/***/ }),

/***/ "./src/app/services/irrigationZones.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IrrigationZoneService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IrrigationZoneService = /** @class */ (function () {
    function IrrigationZoneService(http) {
        this.http = http;
        this.dataStore = { irrigationZones: [] };
        this.allIrrigationZones = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]([]);
        this.irrigationZones = this.allIrrigationZones.asObservable();
    }
    IrrigationZoneService.prototype.loadAll = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        this.http.get('http://localhost:3000/zones/zoneList', { headers: headers })
            .subscribe(function (data) {
            _this.dataStore.irrigationZones = data.json();
            _this.allIrrigationZones.next(Object.assign({}, _this.dataStore).irrigationZones);
        }, function (error) { return console.log('Could not Irrigation Zones'); });
    };
    IrrigationZoneService.prototype.create = function (zone) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        this.http.post('http://localhost:3000/zones/zone', zone, { headers: headers })
            .subscribe(function (data) {
            _this.dataStore.irrigationZones.push(data.json());
            _this.allIrrigationZones.next(Object.assign({}, _this.dataStore).irrigationZones);
        }, function (error) { return console.log('Could not create Irrigation Zone'); });
    };
    IrrigationZoneService.prototype.update = function (zone) {
        var _this = this;
        console.log(zone);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-type', 'application/json');
        this.http.put('http://localhost:3000/zones/zone/' + zone._id, zone)
            .subscribe(function (data) {
            _this.dataStore.irrigationZones.forEach(function (issue, i) {
                if (issue._id === data.json()._id) {
                    _this.dataStore.irrigationZones[i] = data.json();
                }
            });
            _this.allIrrigationZones.next(Object.assign({}, _this.dataStore).irrigationZones);
        }, function (error) { return console.log('Could not Update!'); });
    };
    IrrigationZoneService.prototype.remove = function (issueId) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-type', 'application/json');
        this.http.delete('http://localhost:3000/zones/zone/' + issueId)
            .subscribe(function (response) {
            _this.dataStore.irrigationZones.forEach(function (issue, i) {
                if (issue._id === issueId) {
                    _this.dataStore.irrigationZones.splice(i, 1);
                }
            });
            _this.allIrrigationZones.next(Object.assign({}, _this.dataStore).irrigationZones);
        }, function (error) { return console.log('could not remove'); });
    };
    IrrigationZoneService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], IrrigationZoneService);
    return IrrigationZoneService;
}());



/***/ }),

/***/ "./src/app/services/logs.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LogService = /** @class */ (function () {
    function LogService(http) {
        this.http = http;
        this.dataStore = { issues: [] };
        this.issueLogs = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]([]);
        this.issues = this.issueLogs.asObservable();
    }
    LogService.prototype.loadAll = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        this.http.get('http://localhost:3000/issueLogs/issueList', { headers: headers })
            .subscribe(function (data) {
            _this.dataStore.issues = data.json();
            _this.issueLogs.next(Object.assign({}, _this.dataStore).issues);
        }, function (error) { return console.log('Could not Load logs'); });
    };
    LogService.prototype.create = function (issue) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        this.http.post('http://localhost:3000/issueLogs/issue', issue, { headers: headers })
            .subscribe(function (data) {
            _this.dataStore.issues.push(data.json());
            _this.issueLogs.next(Object.assign({}, _this.dataStore).issues);
        }, function (error) { return console.log('well shit'); });
    };
    LogService.prototype.update = function (issue) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-type', 'application/json');
        this.http.put('http://localhost:3000/issueLogs/issue/' + issue._id, issue)
            .subscribe(function (data) {
            _this.dataStore.issues.forEach(function (issue, i) {
                if (issue._id === data.json()._id) {
                    _this.dataStore.issues[i] = data.json();
                }
            });
            _this.issueLogs.next(Object.assign({}, _this.dataStore).issues);
        }, function (error) { return console.log('Could not update!'); });
    };
    LogService.prototype.remove = function (issueId) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-type', 'application/json');
        this.http.delete('http://localhost:3000/issueLogs/issue/' + issueId)
            .subscribe(function (response) {
            _this.dataStore.issues.forEach(function (issue, i) {
                if (issue._id === issueId) {
                    _this.dataStore.issues.splice(i, 1);
                }
            });
            _this.issueLogs.next(Object.assign({}, _this.dataStore).issues);
        }, function (error) { return console.log('could not remove'); });
    };
    LogService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], LogService);
    return LogService;
}());



/***/ }),

/***/ "./src/app/services/message.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MessageService = /** @class */ (function () {
    function MessageService() {
        this.subject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["a" /* Subject */]();
    }
    MessageService.prototype.sendMessage = function (msg) {
        this.subject.next({ text: msg });
    };
    MessageService.prototype.clearMessage = function () {
        this.subject.next();
    };
    MessageService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    MessageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], MessageService);
    return MessageService;
}());



/***/ }),

/***/ "./src/app/services/users.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UsersService = /** @class */ (function () {
    function UsersService(http) {
        this.http = http;
        this.dataStore = { users: [] };
        this.allUsers = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]([]);
        this.users = this.allUsers.asObservable();
    }
    UsersService.prototype.loadAll = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        this.http.get('http://localhost:3000/users/usersList', { headers: headers })
            .subscribe(function (data) {
            _this.dataStore.users = data.json();
            _this.allUsers.next(Object.assign({}, _this.dataStore).users);
        }, function (error) { return console.log(error); });
    };
    UsersService.prototype.create = function (user) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        this.http.post('http://localhost/3000/users/register', user, { headers: headers })
            .subscribe(function (data) {
            _this.dataStore.users.push(data.json());
            _this.allUsers.next(Object.assign({}, _this.dataStore).users);
        }, function (error) { return console.log(error); });
    };
    /*
    update(user : User) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.put('http://localhost:3000/users/useri/'+user._id, user)
    .subscribe(data => {
    this.dataStore.users.forEach((user, i) => {
    if(user._id === data.json()._id) { this.dataStore.users[i] = data.json(); }
});
this.allUsers.next(Object.assign({}, this.dataStore).users);
} error => console.log(error));
}*/
    UsersService.prototype.remove = function (userId) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/json');
        this.http.delete('http://localhost:3000/users/user/' + userId)
            .subscribe(function (response) {
            _this.dataStore.users.forEach(function (user, i) {
                if (user._id === userId) {
                    _this.dataStore.users.splice(i, 1);
                }
            });
            _this.allUsers.next(Object.assign({}, _this.dataStore).users);
        }, function (error) { return console.log(error); });
    };
    UsersService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/app/services/validate.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ValidateService = /** @class */ (function () {
    function ValidateService() {
    }
    ValidateService.prototype.validateRegister = function (user) {
        if (user.name == undefined || user.email == undefined ||
            user.username == undefined || user.password == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateEmail = function (email) {
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    };
    ValidateService.prototype.validateIssueLog = function (issue) {
        if (issue.name == undefined || issue.severity == undefined
            || issue.dateCreated == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ValidateService);
    return ValidateService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_hammerjs__ = __webpack_require__("./node_modules/hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("./src/environments/environment.ts");





if (__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map